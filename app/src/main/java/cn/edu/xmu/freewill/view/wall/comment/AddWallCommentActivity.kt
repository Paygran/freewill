package cn.edu.xmu.freewill.view.wall.comment

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import cn.bmob.v3.BmobUser
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.SaveListener
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.FreeWillUser
import cn.edu.xmu.freewill.data.Wall
import cn.edu.xmu.freewill.data.WallComment

import kotlinx.android.synthetic.main.activity_add_wall_comment.*
import kotlinx.android.synthetic.main.content_add_wall_comment.*

class AddWallCommentActivity : AppCompatActivity() {
    private var wallId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_wall_comment)
        setSupportActionBar(toolbar)

        wallId = intent.getStringExtra(ARG_WALL_ID)
        if (wallId == null) {
            finish()
            return
        }

        fabSend.setOnClickListener {
            val commentText = commentEditText.text.toString().trim()
            if (commentText.isEmpty()) return@setOnClickListener
            val comment = WallComment()
            val wall = Wall()
            wall.objectId = wallId
            comment.wall = wall
            comment.comment = commentText
            comment.owner = BmobUser.getCurrentUser(FreeWillUser::class.java)
            comment.save(object : SaveListener<String>() {
                override fun done(id: String?, e: BmobException?) {
                    if (e == null) {
                        finish()
                    } else {
                        Log.e(TAG, e.message)
                        Toast.makeText(this@AddWallCommentActivity, "Comment failed, maybe some network problem.", Toast.LENGTH_LONG)
                                .show()
                    }
                }

            })
        }
    }

    companion object {
        private val TAG = AddWallCommentActivity::class.simpleName
        val ARG_WALL_ID = "wall"
    }

}
