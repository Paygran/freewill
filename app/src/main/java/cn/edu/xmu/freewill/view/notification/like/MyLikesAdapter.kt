package cn.edu.xmu.freewill.view.notification.like

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Handler
import android.os.Looper
import android.support.v7.widget.RecyclerView
import android.util.LruCache
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.WallLike
import cn.edu.xmu.freewill.utils.loadThumbnail
import cn.edu.xmu.freewill.view.common.PaginationRecyclerViewAdapter
import com.makeramen.roundedimageview.RoundedImageView

/**
 * Created by eason on 2018/1/14.
 */
abstract class MyLikesAdapter : PaginationRecyclerViewAdapter<WallLike>() {
    private val mMemoryCache: LruCache<String, Bitmap>

    init {
        val maxMemory = Runtime.getRuntime().maxMemory() / 1024
        val cacheSize = maxMemory / 8
        mMemoryCache = object : LruCache<String, Bitmap>(cacheSize.toInt()) {
            override fun sizeOf(key: String?, bitmap: Bitmap?): Int =
                    if (bitmap == null) 0 else bitmap.byteCount / 1024
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val like = getDataByPosition(position)
        if (like != null) {
            holder as LikeViewHolder
            holder.usernameView.text = like.user?.username
            holder.timestampView.text = like.createdAt
            val avatar = mMemoryCache[like.user?.objectId]
            if (avatar != null) {
                holder.avatarView.setImageBitmap(avatar)
            } else {
                like.user?.avatar?.loadThumbnail(holder.itemView.context, like.user!!.objectId, {
                    Thread({
                        val ava = BitmapFactory.decodeFile(it.absolutePath)
                        if (ava != null) {
                            mMemoryCache.put(like.user?.objectId, ava)
                            Handler(Looper.getMainLooper()).post {
                                holder.avatarView.setImageBitmap(ava)
                            }
                        }
                    }).start()
                })
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType != PaginationRecyclerViewAdapter.VIEW_TYPE_LOADING) {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_wall_like, parent, false)
            return LikeViewHolder(v)
        }
        return super.onCreateViewHolder(parent, viewType)
    }

    inner class LikeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            ButterKnife.bind(this, itemView)
        }

        @BindView(R.id.avatarView)
        lateinit var avatarView: RoundedImageView

        @BindView(R.id.userNameView)
        lateinit var usernameView: TextView

        @BindView(R.id.timeStampView)
        lateinit var timestampView: TextView
    }
}