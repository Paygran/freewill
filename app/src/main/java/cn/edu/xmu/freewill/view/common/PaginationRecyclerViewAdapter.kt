package cn.edu.xmu.freewill.view.common

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import butterknife.BindView
import butterknife.ButterKnife
import cn.edu.xmu.freewill.R

/**
 * Created by eason on 2018/1/14.
 */
abstract class PaginationRecyclerViewAdapter<DataType> : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mData = mutableListOf<DataType?>()
    private var mCurrPage = -1
    var mIsLoading = false
        set(value) {
            if (value) addLoadingView()
            else removeLoadingView()
            field = value
        }

    abstract fun loadData(pageNum: Int)

    private fun addLoadingView() {
        mData.add(null)
        notifyItemInserted(mData.lastIndex)
    }

    private fun removeLoadingView() {
        if (mData.isNotEmpty() && mData.last() == null) {
            mData.removeAt(mData.lastIndex)
            notifyItemRemoved(mData.lastIndex + 1)
        }
    }

    override fun getItemCount(): Int =
            mData.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_loading, parent, false)

        if (viewType != VIEW_TYPE_LOADING) {
            v.visibility = View.GONE
        }
        return LoadingViewHolder(v)
    }

    override fun getItemViewType(position: Int): Int {
        if (mData[position] == null) {
            return VIEW_TYPE_LOADING
        }
        return VIEW_TYPE_ITEM
    }

    fun getDataByPosition(position: Int) =
            mData[position]

    fun onDoneQuery(pageNum: Int, data: MutableList<DataType>) {
        mIsLoading = false
        if (data.isNotEmpty()) {
            if (pageNum <= mCurrPage) mData.clear()
            mCurrPage = pageNum
            mData.addAll(data)
            notifyItemRangeInserted(mData.size - data.size, data.size)
        }
    }

    fun onQueryFailed() {
        mIsLoading = false
    }

    fun loadMore() {
        mIsLoading = true
        loadData(mCurrPage + 1)
    }

    inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            ButterKnife.bind(this, itemView)
        }

        @BindView(R.id.progressBar)
        lateinit var progressBar: ProgressBar
    }

    companion object {
        const val VIEW_TYPE_LOADING = 0
        const val VIEW_TYPE_ITEM = 1
    }
}