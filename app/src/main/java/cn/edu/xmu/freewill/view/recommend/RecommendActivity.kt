package cn.edu.xmu.freewill.view.recommend

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.QueryDoneListener
import cn.edu.xmu.freewill.data.RemoteDataQueryObj
import cn.edu.xmu.freewill.data.SpotRecommend

class RecommendActivity : AppCompatActivity() {
    private lateinit var mSpotId: String
    private lateinit var mSpotName: String

    private lateinit var mRecommendList: RecyclerView
    private lateinit var mProgressBar: ProgressBar

    private var mRecommendsAdapter: RecommendsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val id = intent.getStringExtra(ARG_SPOT_ID)
        if (id == null) {
            Log.e(TAG, "Unable to start $TAG, out of null spot id extra!")
            finish()
            return
        }
        mSpotId = id
        mSpotName = intent.getStringExtra(ARG_SPOT_NAME)

        setContentView(R.layout.activity_recommend)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        mProgressBar = findViewById<ProgressBar>(R.id.progressBar)
        mRecommendList = findViewById<RecyclerView>(R.id.recommend_list)
        mRecommendList.layoutManager = LinearLayoutManager(this)
        loadData()
    }

    private fun loadData() {
        showProgress(true)
        supportActionBar!!.title = getString(R.string.title_activity_recommend).format(mSpotName)
        RemoteDataQueryObj.querySpotRecommend(mSpotId, 0, 99, object : QueryDoneListener<SpotRecommend> {
            override fun onSuccess(data: MutableList<SpotRecommend>?) {
                if (data != null) {
                    mRecommendsAdapter = RecommendsAdapter(data)
                    mRecommendList.adapter = mRecommendsAdapter
                }
                showProgress(false)
            }

            override fun onFailed(e: Exception) {
                Toast.makeText(this@RecommendActivity, "查找小萱推荐失败", Toast.LENGTH_LONG)
                        .show()
                showProgress(false)
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item?.itemId) {
        android.R.id.home -> {
            this.finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun showProgress(show: Boolean) {
        mProgressBar.visibility = if (show) View.VISIBLE else View.GONE
        mRecommendList.visibility = if (show) View.GONE else View.VISIBLE
    }

    companion object {
        const val ARG_SPOT_ID = "Spot Id"
        const val ARG_SPOT_NAME = "Spot Name"
        private val TAG = RecommendActivity::class.simpleName
    }
}
