package cn.edu.xmu.freewill.data

import cn.bmob.v3.BmobObject
import cn.bmob.v3.BmobUser
import cn.bmob.v3.datatype.BmobFile
import cn.bmob.v3.datatype.BmobGeoPoint
import cn.bmob.v3.helper.GsonUtil
import com.raizlabs.android.dbflow.annotation.*
import com.raizlabs.android.dbflow.converter.TypeConverter
import org.json.JSONObject

/**
 * Created by paygran on 17-8-21.
 */
@Table(database = FreewillDB::class,
        name = "Spot",
        inheritedColumns = [(InheritedColumn(column = Column(), fieldName = "createAt")), (InheritedColumn(column = Column(), fieldName = "updateAt"))],
        inheritedPrimaryKeys = [(InheritedPrimaryKey(primaryKey = PrimaryKey(), column = Column(), fieldName = "objectId"))])
data class Spot(@Column(name = "name") var name: String? = null,
                @Column(name = "introduction") var introduction: String? = null,
                @Column(name = "click_times") var clickTimes: Int = 0,
                @Column(name = "location", typeConverter = GeoPointConverter::class) var location: BmobGeoPoint? = null,
                @Column(name = "image", typeConverter = BmobFileConverter::class) var image: BmobFile? = null) : BmobObject()

@Table(database = FreewillDB::class,
        name = "SpotImage",
        inheritedColumns = [(InheritedColumn(column = Column(), fieldName = "createAt")), (InheritedColumn(column = Column(), fieldName = "updateAt"))],
        inheritedPrimaryKeys = [(InheritedPrimaryKey(primaryKey = PrimaryKey(), column = Column(), fieldName = "objectId"))])
data class SpotImage(@Column(name = "spot", typeConverter = SubSpotConverter::class) var spot: SubSpot? = null,
                     @Column(name = "image", typeConverter = BmobFileConverter::class) var image: BmobFile? = null) : BmobObject()

@Table(database = FreewillDB::class,
        name = "FreewillUser",
        inheritedColumns = [(InheritedColumn(column = Column(), fieldName = "createAt")),
            (InheritedColumn(column = Column(), fieldName = "updateAt")),
            (InheritedColumn(column = Column(), fieldName = "username"))],
        inheritedPrimaryKeys = [(InheritedPrimaryKey(primaryKey = PrimaryKey(), column = Column(), fieldName = "objectId"))])
data class FreeWillUser(@Column(name = "avatarView", typeConverter = BmobFileConverter::class) var avatar: BmobFile? = null) : BmobUser()

@Table(database = FreewillDB::class,
        name = "SpotRecommend",
        inheritedColumns = [(InheritedColumn(column = Column(), fieldName = "createAt")), (InheritedColumn(column = Column(), fieldName = "updateAt"))],
        inheritedPrimaryKeys = [(InheritedPrimaryKey(primaryKey = PrimaryKey(), column = Column(), fieldName = "objectId"))])
data class SpotRecommend(@Column(name = "name") var name: String? = null,
                         @Column(name = "description") var description: String? = null,
                         @Column(name = "spot", typeConverter = SpotConverter::class) var spot: Spot? = null) : BmobObject()

data class SpotRecommendImage(var spotRecommend: SpotRecommend = SpotRecommend(),
                              var image: BmobFile? = null) : BmobObject()

@Table(database = FreewillDB::class,
        name = "ImageLoading",
        inheritedColumns = [(InheritedColumn(column = Column(), fieldName = "createAt")), (InheritedColumn(column = Column(), fieldName = "updateAt"))],
        inheritedPrimaryKeys = [(InheritedPrimaryKey(primaryKey = PrimaryKey(), column = Column(), fieldName = "objectId"))])
data class ImageLoading(@Column(name = "image", typeConverter = BmobFileConverter::class) var image: BmobFile? = null) : BmobObject()

@Table(database = FreewillDB::class,
        name = "SubSpot",
        inheritedColumns = [(InheritedColumn(column = Column(), fieldName = "createAt")), (InheritedColumn(column = Column(), fieldName = "updateAt"))],
        inheritedPrimaryKeys = [(InheritedPrimaryKey(primaryKey = PrimaryKey(), column = Column(), fieldName = "objectId"))])
data class SubSpot(@Column(name = "belonging", typeConverter = SpotConverter::class) var belonging: Spot = Spot(),
                   @Column(name = "name") var name: String = "",
                   @Column(name = "introduction") var introduction: String = "",
                   @Column(name = "locationX") var locationX: Float = 0f,
                   @Column(name = "locationY") var locationY: Float = 0f) : BmobObject()

@Table(database = FreewillDB::class,
        name = "Wall",
        inheritedColumns = [(InheritedColumn(column = Column(), fieldName = "createAt")), (InheritedColumn(column = Column(), fieldName = "updateAt"))],
        inheritedPrimaryKeys = [(InheritedPrimaryKey(primaryKey = PrimaryKey(), column = Column(), fieldName = "objectId"))])
data class Wall(@Column(name = "user", typeConverter = FreeWillUserConverter::class) var user: FreeWillUser? = null,
                @Column(name = "message") var message: String = "") : BmobObject()

@Table(database = FreewillDB::class,
        name = "WallComment",
        inheritedColumns = [(InheritedColumn(column = Column(), fieldName = "createAt")), (InheritedColumn(column = Column(), fieldName = "updateAt"))],
        inheritedPrimaryKeys = [(InheritedPrimaryKey(primaryKey = PrimaryKey(), column = Column(), fieldName = "objectId"))])
data class WallComment(@Column(name = "comment") var comment: String = "",
                       @Column(name = "owner", typeConverter = FreeWillUserConverter::class) var owner: FreeWillUser? = null,
                       @Column(name = "wall", typeConverter = WallConverter::class) var wall: Wall? = null) : BmobObject()

@Table(database = FreewillDB::class,
        name = "WallLike",
        inheritedColumns = [(InheritedColumn(column = Column(), fieldName = "createAt")), (InheritedColumn(column = Column(), fieldName = "updateAt"))],
        inheritedPrimaryKeys = [(InheritedPrimaryKey(primaryKey = PrimaryKey(), column = Column(), fieldName = "objectId"))])
data class WallLike(@Column(name = "wall", typeConverter = WallConverter::class) var wall: Wall? = null,
                    @Column(name = "user", typeConverter = FreeWillUserConverter::class) var user: FreeWillUser? = null) : BmobObject()

data class _Article(
        var url: String? = null,
        var type: String? = null,
        var title: String? = null,
        var m_id: String? = null,
        var desc: String? = null,
        var cover: String? = null,
        var content: String? = null
) : BmobObject()

@com.raizlabs.android.dbflow.annotation.TypeConverter
class WallConverter : TypeConverter<String, Wall>() {
    override fun getDBValue(model: Wall?): String =
            GsonUtil.toJson(model)

    override fun getModelValue(data: String?): Wall =
            GsonUtil.toObject(data, Wall::class.java) as Wall

}

@com.raizlabs.android.dbflow.annotation.TypeConverter
class FreeWillUserConverter : TypeConverter<String, FreeWillUser>() {
    override fun getDBValue(model: FreeWillUser?): String {
        if (model == null) return ""
        val json = JSONObject()
        json.put("username", model.username)
        json.put("objectId", model.objectId)
        return json.toString()
    }

    override fun getModelValue(data: String?): FreeWillUser {
        val json = JSONObject(data)
        val user = FreeWillUser()
        user.objectId = json["objectId"] as String
        user.username = json["username"] as String
        return user
    }

}

@com.raizlabs.android.dbflow.annotation.TypeConverter
class GeoPointConverter : TypeConverter<String, BmobGeoPoint>() {
    override fun getDBValue(model: BmobGeoPoint?): String =
            GsonUtil.toJson(model)

    override fun getModelValue(data: String?): BmobGeoPoint? =
            GsonUtil.toObject(data, BmobGeoPoint::class.java) as BmobGeoPoint?
}

@com.raizlabs.android.dbflow.annotation.TypeConverter
class SpotConverter : TypeConverter<String, Spot>() {
    override fun getDBValue(model: Spot?): String?
            = GsonUtil.toJson(model)

    override fun getModelValue(data: String?): Spot? =
            GsonUtil.toObject(data, Spot::class.java) as Spot?
}

@com.raizlabs.android.dbflow.annotation.TypeConverter
class BmobFileConverter : TypeConverter<String, BmobFile>() {
    override fun getDBValue(model: BmobFile?): String? =
            GsonUtil.toJson(model)

    override fun getModelValue(data: String?): BmobFile? =
            GsonUtil.toObject(data, BmobFile::class.java) as BmobFile?
}

@com.raizlabs.android.dbflow.annotation.TypeConverter
class SubSpotConverter : TypeConverter<String, SubSpot>() {
    override fun getModelValue(data: String?): SubSpot? =
            GsonUtil.toObject(data, SubSpot::class.java) as SubSpot?

    override fun getDBValue(model: SubSpot?): String? =
            GsonUtil.toJson(model)

}