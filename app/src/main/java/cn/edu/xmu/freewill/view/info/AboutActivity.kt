package cn.edu.xmu.freewill.view.info

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import cn.edu.xmu.freewill.R
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val pInfo = packageManager.getPackageInfo(packageName, 0)
        appName.text = (getString(R.string.name_app_version).format(getString(R.string.app_name), pInfo.versionName))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            finish()
        }
        return true
    }

    companion object {
        private val TAG = AboutActivity::class.simpleName
    }
}