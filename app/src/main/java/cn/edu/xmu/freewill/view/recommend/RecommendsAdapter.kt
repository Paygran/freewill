package cn.edu.xmu.freewill.view.recommend

import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.QueryDoneListener
import cn.edu.xmu.freewill.data.RemoteDataQueryObj
import cn.edu.xmu.freewill.data.SpotRecommend
import cn.edu.xmu.freewill.data.SpotRecommendImage
import cn.edu.xmu.freewill.utils.loadThumbnail

/**
 * Created by eason on 2017/9/23.
 */
class RecommendsAdapter(recommends: MutableList<SpotRecommend>) : RecyclerView.Adapter<RecommendsAdapter.RecommendViewHolder>() {
    private var mRecommends: MutableList<SpotRecommend> = recommends

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecommendViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_recommend_list, parent, false)
        return RecommendViewHolder(v)
    }

    override fun onBindViewHolder(holder: RecommendViewHolder, position: Int) {
        val recommend = mRecommends[position]
        holder.nameTextView.text = recommend.name
        holder.introTextView.text = recommend.description
        holder.detailsShow.setOnClickListener {
            if (holder.isDrop) {
                holder.detailsShow.setImageResource(R.drawable.ic_arrow_drop_down)
                holder.introTextView.visibility = View.GONE
            } else {
                holder.detailsShow.setImageResource(R.drawable.ic_arrow_drop_up)
                holder.introTextView.visibility = View.VISIBLE
            }
            holder.isDrop = !holder.isDrop
        }
        RemoteDataQueryObj.querySpotRecommendImage(recommend.objectId, 0, 1,
                object : QueryDoneListener<SpotRecommendImage> {
                    override fun onFailed(e: Exception) {

                    }

                    override fun onSuccess(data: MutableList<SpotRecommendImage>?) {
                        if (data != null && data.isNotEmpty()) {
                            val image = data[0]
                            image.image?.loadThumbnail(holder.itemView.context,
                                    image.objectId,
                                    holder.recommendImageView.width,
                                    holder.recommendImageView.height,
                                    { holder.recommendImageView.setImageURI(Uri.fromFile(it)) })
                        }
                    }

                })
    }

    override fun getItemCount(): Int =
            mRecommends.size

    class RecommendViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val recommendImageView: ImageView = itemView.findViewById(R.id.recommend_image)
        val introTextView: TextView = itemView.findViewById(R.id.intro_textView)
        val nameTextView: TextView = itemView.findViewById(R.id.name_textView)
        val detailsShow: ImageButton = itemView.findViewById(R.id.detailsShow)
        var isDrop = false
    }
}