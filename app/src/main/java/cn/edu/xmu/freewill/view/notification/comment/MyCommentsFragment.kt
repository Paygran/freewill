package cn.edu.xmu.freewill.view.notification.comment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.QueryDoneListener
import cn.edu.xmu.freewill.data.RemoteDataQueryObj
import cn.edu.xmu.freewill.data.WallComment
import cn.edu.xmu.freewill.view.common.ListEndlessOnScrollListener
import cn.edu.xmu.freewill.view.common.Refreshable
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_wall_comment_list.*

/**
 * Created by eason on 2018/1/14.
 */
class MyCommentsFragment : Fragment(), Refreshable {

    private val mMyCommentAdapter: MyCommentAdapter = object : MyCommentAdapter() {
        override fun loadData(pageNum: Int) {
            RemoteDataQueryObj.queryMyComments(pageNum, 20, object : QueryDoneListener<WallComment> {
                override fun onFailed(e: Exception) {
                    mOnRefresh()
                    Log.e(TAG, e.message)
                    onQueryFailed()
                }

                override fun onSuccess(data: MutableList<WallComment>?) {
                    mOnRefresh()
                    if (data != null) {
                        onDoneQuery(pageNum, data)
                    } else {
                        Log.e(TAG, "Query data is null")
                        onQueryFailed()
                    }
                }

            })
        }

    }
    private var mOnRefresh: () -> Unit = {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_wall_comment_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManger = LinearLayoutManager(context)
        commentList.layoutManager = layoutManger
        commentList.addItemDecoration(DividerItemDecoration(context, layoutManger.orientation))
        commentList.adapter = mMyCommentAdapter
        commentList.addOnScrollListener(object : ListEndlessOnScrollListener(layoutManger) {
            override fun isLoading(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>): Boolean =
                    mMyCommentAdapter.mIsLoading

            override fun onLoadMore(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
                mMyCommentAdapter.loadMore()
            }

        })
        mMyCommentAdapter.loadMore()
    }

    override fun onDestroy() {
        this.clearFindViewByIdCache()
        super.onDestroy()
    }

    override fun refresh(doneRefresh: () -> Unit) {
        mOnRefresh = doneRefresh
        mMyCommentAdapter.loadData(0)
    }

    companion object {
        private val TAG = MyCommentsFragment::class.simpleName
        fun newInstance(): MyCommentsFragment =
                MyCommentsFragment()
    }
}