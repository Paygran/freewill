package cn.edu.xmu.freewill.view.spot.detail

import android.content.Intent
import android.graphics.Point
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.QueryDoneListener
import cn.edu.xmu.freewill.data.RemoteDataQueryObj
import cn.edu.xmu.freewill.data.Spot
import cn.edu.xmu.freewill.data.SubSpot
import cn.edu.xmu.freewill.utils.loadThumbnail
import cn.edu.xmu.freewill.view.recommend.RecommendActivity
import cn.edu.xmu.freewill.view.spot.SpotConstants
import com.iflytek.cloud.ErrorCode
import com.iflytek.cloud.SpeechError
import com.iflytek.cloud.SpeechSynthesizer
import com.iflytek.cloud.SynthesizerListener
import kotlinx.android.synthetic.main.activity_spot_detail.*
import kotlinx.android.synthetic.main.content_spot_detail.*
import kotlin.math.absoluteValue

class SpotDetailActivity : AppCompatActivity(), SynthesizerListener {

    private lateinit var mId: String

    private lateinit var mSpot: Spot

    private val mSubSpots = mutableListOf<SubSpot>()

    private var mTts: SpeechSynthesizer? = null

    private var mSpokenText = ""

    private var mPaused = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spot_detail)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        initData()
        RemoteDataQueryObj.spotClickTimesPlus(mId, 1)
        mTts = SpeechSynthesizer.createSynthesizer(this) { code ->
            if (code != ErrorCode.SUCCESS) {
                Log.e(TAG, "Initialize SpeechSynthesizer Failed")
                mTts = null
            }
        }

        actionPlay.setOnClickListener {
            playVoice(mSpokenText)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_spot_detail, menu)
        return true
    }

    private fun initData() {
        mId = intent.getStringExtra(SpotConstants.ID)
        RemoteDataQueryObj.querySpot(mId, object : QueryDoneListener<Spot> {
            override fun onSuccess(data: MutableList<Spot>?) {
                mSpot = data!![0]
                mSpokenText = mSpot.introduction.toString()
                loadView()
            }

            override fun onFailed(e: Exception) {
                finish()
            }
        })
        RemoteDataQueryObj.querySubSpot(mId, object : QueryDoneListener<SubSpot> {
            override fun onFailed(e: Exception) {
                Log.e(TAG, e.message, e)
            }

            override fun onSuccess(data: MutableList<SubSpot>?) {
                if (data != null) {
                    mSubSpots.addAll(data)
                    markupSubSpots()
                }
            }

        })
    }

    private fun markupSubSpots() {
        val subSpotMarkers = mutableListOf<View>()
        mSubSpots.forEach { subSpot ->
            val markerView = layoutInflater.inflate(R.layout.item_sub_spot_mark, appBarContent, false)
            subSpotMarkers.add(markerView)
            val markerButton = markerView.findViewById<Button>(R.id.marker)
            markerButton.text = subSpot.name
            markerButton.setOnClickListener {
                spotIntroView.text = subSpot.introduction
                if (mTts?.isSpeaking!!) {
                    mTts?.stopSpeaking()
                }
                mSpokenText = subSpot.introduction
                playVoice(mSpokenText)
            }
            val layoutParams = markerView.layoutParams as CollapsingToolbarLayout.LayoutParams
            val leftMargin = (appBarContent.width * subSpot.locationX).toInt()
            val topMargin = (appBarContent.height * subSpot.locationY).toInt()
            val display = windowManager.defaultDisplay
            val size = Point()
            display.getSize(size)
            markerView.measure(size.x, size.y)
            layoutParams.leftMargin = if (leftMargin + markerView.measuredWidth > appBarContent.width)
                appBarContent.width - markerView.measuredWidth else leftMargin
            layoutParams.topMargin = if (topMargin + markerView.measuredHeight > appBarContent.height)
                appBarContent.height - markerView.measuredHeight else topMargin
            appBarContent.addView(markerView, layoutParams)
        }
        appbar.addOnOffsetChangedListener({ appBarLayout, verticalOffset ->
            Log.i(TAG, verticalOffset.toString())
            if (verticalOffset.absoluteValue - appBarLayout.totalScrollRange == 0) {
                subSpotMarkers.forEach { it.visibility = View.GONE }
                supportActionBar?.setDisplayShowTitleEnabled(true)
            } else {
                subSpotMarkers.forEach { it.visibility = View.VISIBLE }
                supportActionBar?.setDisplayShowTitleEnabled(false)
            }
        })
    }

    override fun onBackPressed() {
        if (mTts!!.isSpeaking) {
            mTts?.stopSpeaking()
        }
        if (spotIntroView.text.toString() == mSpot.introduction) {
            super.onBackPressed()
        } else {
            spotIntroView.text = mSpot.introduction
            mSpokenText = mSpot.introduction.toString()
            onCompleted(null)
        }
    }

    private fun loadView() {
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.title = mSpot.name
        spotIntroView.text = mSpot.introduction
        mSpot.image?.loadThumbnail(this, mId,
                spotImageView.width, spotImageView.height,
                { spotImageView.setImageURI(Uri.fromFile(it)) })
    }

    private fun playVoice(text: String) {
        if (mTts != null) {
            if (!mTts?.isSpeaking!!) {
                mTts?.startSpeaking(text, this)
            } else {
                if (mPaused) {
                    actionPlay.setImageResource(R.drawable.ic_pause_black)
                    mPaused = false
                    mTts?.resumeSpeaking()
                } else {
                    actionPlay.setImageResource(R.drawable.ic_play_arrow_black)
                    mPaused = true
                    mTts?.pauseSpeaking()
                }
            }
            return
        }
        mTts = SpeechSynthesizer.createSynthesizer(this) { code ->
            if (code != ErrorCode.SUCCESS) {
                Log.e(TAG, "Initialize SpeechSynthesizer Failed")
            } else {
                mTts?.startSpeaking(text, this)
                actionPlay.setImageResource(R.drawable.ic_pause_black)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item?.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.action_recommend -> {
            val intent = Intent(this, RecommendActivity::class.java)
            intent.putExtra(RecommendActivity.ARG_SPOT_NAME, mSpot.name)
            intent.putExtra(RecommendActivity.ARG_SPOT_ID, mId)
            startActivity(intent)
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        mTts?.stopSpeaking()
        mTts?.destroy()
        super.onDestroy()
    }

    override fun onPause() {
        mTts?.pauseSpeaking()
        super.onPause()
    }

    override fun onResume() {
        //mTts?.resumeSpeaking()
        super.onResume()
    }

    override fun onBufferProgress(p0: Int, p1: Int, p2: Int, p3: String?) {

    }

    override fun onSpeakBegin() {
        actionPlay.setImageResource(R.drawable.ic_pause_black)
        mPaused = false
    }

    override fun onSpeakProgress(percent: Int, beginPos: Int, endPos: Int) {
        progress.progress = percent
    }

    override fun onEvent(p0: Int, p1: Int, p2: Int, p3: Bundle?) {

    }

    override fun onSpeakPaused() {
        actionPlay.setImageResource(R.drawable.ic_play_arrow_black)
        mPaused = true
    }

    override fun onSpeakResumed() {
        actionPlay.setImageResource(R.drawable.ic_pause_black)
        mPaused = false
    }

    override fun onCompleted(p0: SpeechError?) {
        actionPlay.setImageResource(R.drawable.ic_play_arrow_black)
        progress.progress = 0
    }

//    private inner class SpotImageSlideAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
//        private var mSpotImages: List<SpotImage> = listOf()
//
//        init {
//            RemoteDataQueryObj.querySpotImageRemote(0, 100, mId, object : QueryDoneListener<SpotImage> {
//                override fun onSuccess(data: MutableList<SpotImage>?) {
//                    if (data != null) {
//                        mSpotImages = data
//                        notifyDataSetChanged()
//                    }
//                }
//
//                override fun onFailed(e: Exception) {
//                    Log.e(TAG, e.message)
//                }
//
//            })
//        }
//
//        override fun getItem(position: Int): Fragment {
//            val spotImage = mSpotImages[position]
//            var url = spotImage.image?.url
//            if (url == null) url = ""
//            return ImageFragment.newInstance(spotImage.objectId, url)
//        }
//
//        override fun getCount(): Int = mSpotImages.size
//
//    }
companion object {
    private val TAG = SpotDetailActivity::class.simpleName
}
}
