package cn.edu.xmu.freewill.view.main

import android.Manifest
import android.animation.ValueAnimator
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Rect
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.AppBarLayout
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import cn.bmob.v3.BmobUser
import cn.bmob.v3.datatype.BmobFile
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.helper.GsonUtil
import cn.bmob.v3.listener.FetchUserInfoListener
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.FreeWillUser
import cn.edu.xmu.freewill.data.QueryDoneListener
import cn.edu.xmu.freewill.data.RemoteDataQueryObj
import cn.edu.xmu.freewill.data.SpotImage
import cn.edu.xmu.freewill.utils.PermissionsUtils
import cn.edu.xmu.freewill.utils.loadThumbnail
import cn.edu.xmu.freewill.view.home.HomepageFragment
import cn.edu.xmu.freewill.view.image.ImageFragment
import cn.edu.xmu.freewill.view.info.AboutActivity
import cn.edu.xmu.freewill.view.info.HelpActivity
import cn.edu.xmu.freewill.view.spot.SpotConstants
import cn.edu.xmu.freewill.view.spot.detail.SpotDetailActivity
import cn.edu.xmu.freewill.view.user.LoginActivity
import cn.edu.xmu.freewill.view.user.SignUpActivity
import cn.edu.xmu.freewill.view.user.UserDetailFragment
import com.google.zxing.integration.android.IntentIntegrator
import com.makeramen.roundedimageview.RoundedImageView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlin.math.absoluteValue


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, HomepageFragment.OnSlideListener, OnFragmentTransition {
    override fun onTransition(title: String) {
        supportActionBar?.title = title
        toolbarCollapse(false)
    }

    private val tag = MainActivity::class.simpleName

    private lateinit var mAvatarView: RoundedImageView

    private var mFragment: Fragment? = null

    private var mUser = BmobUser.getCurrentUser(FreeWillUser::class.java)

    override fun onSlide(position: Int) {
        Log.i(TAG, "Homepage slide to position $position")
        if (position == 0) {
            toolbarCollapse(true)
            //appbar.setExpanded(true, true)
        } else {
            toolbarCollapse(false)
            //appbar.setExpanded(false, true)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initToolbar()

        supportFragmentManager.addOnBackStackChangedListener {
            if (supportFragmentManager.backStackEntryCount < 1) {
                toolbarCollapse(true)
            } else {
                toolbarCollapse(false)
            }
        }

        mFragment = HomepageFragment.newInstance()
        supportFragmentManager.beginTransaction().replace(R.id.fragment_content, mFragment, TAG_HOME_PAGE)
                .commit()

        initDrawer()
        initUser()
        initSpotImages()
    }

    private fun initSpotImages() {
        RemoteDataQueryObj.queryAllSpotImages(0, 5, object : QueryDoneListener<SpotImage> {
            override fun onFailed(e: Exception) {
                Log.e(TAG, e.message)
            }

            override fun onSuccess(data: MutableList<SpotImage>?) {
                if (data == null) return
                spotImagePager.adapter = object : FragmentStatePagerAdapter(this@MainActivity.supportFragmentManager) {
                    override fun getItem(position: Int): Fragment {
                        with(data[position]) {
                            return ImageFragment.newInstance(objectId, image!!.url)
                        }
                    }

                    override fun getCount(): Int = data.size

                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show()
            } else {
                val intent = Intent(this, SpotDetailActivity::class.java)
                intent.putExtra(SpotConstants.ID, result.contents)
                startActivity(intent)
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item!!.itemId) {
        R.id.action_scan -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                PermissionsUtils.checkAndRequestPermission(this, Manifest.permission.CAMERA,
                        R.string.message_request_camera, PermissionsUtils.REQ_CODE_CAMERA
                ) { IntentIntegrator(this).initiateScan() }
            }
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PermissionsUtils.REQ_CODE_CAMERA -> {
                if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    IntentIntegrator(this).initiateScan()
                }
            }
        }
    }

    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        //mAppBarImage = findViewById<ImageView>(R.id.app_bar_image)
        setSupportActionBar(toolbar)
        //supportActionBar?.setDisplayShowTitleEnabled(false)
        //toolbarCollapse(true)
        appbar.addOnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (verticalOffset.absoluteValue - appBarLayout.totalScrollRange == 0) {
                supportActionBar?.setDisplayShowTitleEnabled(true)
            } else {
                supportActionBar?.setDisplayShowTitleEnabled(false)
            }
        }
    }

    private fun toolbarCollapse(isEnabled: Boolean) {
        val appBarParams = appbar.layoutParams
        val p = collapsingToolbarLayout.layoutParams as AppBarLayout.LayoutParams
        var height = resources.getDimensionPixelSize(R.dimen.toolbar_height)
        if (isEnabled) {
            height = resources.getDimensionPixelSize(R.dimen.toolbar_expanded_height)
            p.scrollFlags =
                    AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED
            spotImagePager.visibility = View.VISIBLE
        } else {
            spotImagePager.visibility = View.GONE
            //appBarParams.height = resources.getDimensionPixelSize(R.dimen.toolbar_height)
            p.scrollFlags = 0
        }
        //appbar.layoutParams = appBarParams
        val rect = Rect()
        appbar.getGlobalVisibleRect(rect)
        val animator = ValueAnimator.ofInt(rect.height(), height)
        animator.addUpdateListener {
            appBarParams.height = it.animatedValue as Int
            appbar.layoutParams = appBarParams
        }
        animator.duration = 400
        animator.start()
        collapsingToolbarLayout.layoutParams = p
    }

    private fun initDrawer() {
        val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(this, drawerLayout,
                findViewById<Toolbar>(R.id.toolbar),
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        drawer.setNavigationItemSelectedListener(this)
        val navHeaderView = drawer.getHeaderView(0)
        mAvatarView = navHeaderView.findViewById(R.id.avatar_view)
        drawer.menu.findItem(R.id.nav_homepage).isChecked = true
    }

    fun initUser() {
        mUser = BmobUser.getCurrentUser(FreeWillUser::class.java)
        val navHeaderView = drawer.getHeaderView(0)
        val notLoginContainer = navHeaderView.findViewById<View>(R.id.sign_in_and_sign_up)
        val usernameView = navHeaderView.findViewById<TextView>(R.id.username_view)
        val signInBtn = navHeaderView.findViewById<Button>(R.id.button_sign_in)
        val signUpBtn = navHeaderView.findViewById<Button>(R.id.button_sign_up)
        signInBtn.setOnClickListener {
            val intent = Intent(this@MainActivity, LoginActivity::class.java)
            startActivity(intent)
        }
        signUpBtn.setOnClickListener {
            val intent = Intent(this@MainActivity, SignUpActivity::class.java)
            startActivity(intent)
        }
        if (mUser != null) {
            notLoginContainer.visibility = View.GONE
            usernameView.visibility = View.VISIBLE
            usernameView.text = mUser.username
            syncUser()
        } else {
            notLoginContainer.visibility = View.VISIBLE
            usernameView.visibility = View.GONE
            mAvatarView.setImageResource(R.drawable.ic_person)
        }
    }

    private fun syncUser() {
        BmobUser.fetchUserJsonInfo(object : FetchUserInfoListener<String>() {
            override fun done(s: String?, e: BmobException?) {
                if (e == null) {
                    Log.i(tag, s)
                    mUser = GsonUtil.toObject(s, FreeWillUser::class.java) as FreeWillUser
                    if (mUser.avatar != null) {
                        val thumbnail = BmobFile()
                        thumbnail.url = mUser.avatar!!.url
                        thumbnail.loadThumbnail(this@MainActivity, mUser.objectId) { imageFile ->
                            mAvatarView.setImageURI(Uri.fromFile(imageFile))
                        }
                    }
                } else {
                    Log.e(tag, e.message)
                    if (e.errorCode == 9024) {
                        Log.e(tag, "Not logged in")
                        val intent = Intent(this@MainActivity, LoginActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_actionbar, menu)
        val expandListener = object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                // Do something when action item collapses
                return true // Return true to collapse action view
            }

            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                // Do something when expanded
                return true // Return true to expand action view
            }
        }

        // Get the MenuItem for the action item
        val actionMenuItem = menu?.findItem(R.id.action_search)

        // Assign the listener to that action item
        actionMenuItem?.setOnActionExpandListener(expandListener)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.menu.findItem(id).isChecked) {
            drawerLayout.closeDrawer(GravityCompat.START)
            return true
        }
        when (id) {
            R.id.nav_homepage -> {
                mFragment = HomepageFragment.newInstance()
                supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_content, mFragment, TAG_HOME_PAGE)
                        .commit()
            }

            R.id.nav_footprint -> {

            }

            R.id.nav_account_info -> {
                mFragment = UserDetailFragment.newInstance()
                if (supportFragmentManager.backStackEntryCount > 0) {
                    supportFragmentManager.popBackStack()
                }
                supportFragmentManager.beginTransaction()
                        .hide(supportFragmentManager.findFragmentByTag(TAG_HOME_PAGE))
                        .add(R.id.fragment_content, mFragment, TAG_USER_INFO)
                        .addToBackStack(null)
                        .commit()
            }

//            R.id.nav_feedback -> {
//
//            }
            R.id.nav_about -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
            }

            R.id.nav_help -> {
                val intent = Intent(this, HelpActivity::class.java)
                startActivity(intent)
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onResume() {
        initUser()
        super.onResume()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    companion object {
        private val TAG = MainActivity::class.simpleName
        const val TAG_HOME_PAGE = "homepage"
        const val TAG_USER_INFO = "user info"
        const val TAG_ABOUT = "about"
    }
}
