package cn.edu.xmu.freewill.data

import android.os.Handler
import android.util.Log
import cn.bmob.v3.BmobQuery
import cn.bmob.v3.BmobUser
import cn.bmob.v3.datatype.BmobPointer
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.FindListener
import cn.bmob.v3.listener.QueryListener
import com.raizlabs.android.dbflow.kotlinextensions.async

/**
 * Created by paygran on 17-8-27.
 */
open class RemoteData : LocalData() {
    private val TAG = RemoteData::class.simpleName

    override fun querySpot(pageNum: Int, pageSize: Int, listener: QueryDoneListener<Spot>) {
        super.querySpot(pageNum, pageSize, object : QueryDoneListener<Spot> {
            override fun onFailed(e: Exception) {
                Log.e(TAG, e.message, e)
                querySpotRemote(pageNum, pageSize, listener)
            }

            override fun onSuccess(data: MutableList<Spot>?) {
                listener.onSuccess(data)
                //querySpotRemote(pageNum, pageSize, listener)
            }
        })
    }

    override fun querySpotImage(pageNum: Int, pageSize: Int, spotId: String, listener: QueryDoneListener<SpotImage>) {
        super.querySpotImage(pageNum, pageSize, spotId, object : QueryDoneListener<SpotImage> {
            override fun onFailed(e: Exception) {
                Log.e(TAG, e.message)
                querySpotImageRemote(pageNum, pageSize, spotId, listener)
            }

            override fun onSuccess(data: MutableList<SpotImage>?) {
                listener.onSuccess(data)
                querySpotImageRemote(pageNum, pageSize, spotId, listener)
            }
        })
        //querySpotImageRemote(pageNum, pageSize, spotId, listener)
    }

    override fun querySpot(spotId: String, listener: QueryDoneListener<Spot>) {
        super.querySpot(spotId, object : QueryDoneListener<Spot> {
            override fun onSuccess(data: MutableList<Spot>?) {
                listener.onSuccess(data)
                querySpotRemote(spotId, listener)
            }

            override fun onFailed(e: Exception) {
                Log.e(TAG, e.message)
                querySpotRemote(spotId, listener)
            }
        })
    }

    override fun querySpotRecommend(spotId: String, pageNum: Int, pageSize: Int, listener: QueryDoneListener<SpotRecommend>) {
        super.querySpotRecommend(spotId, pageNum, pageSize, object : QueryDoneListener<SpotRecommend> {
            override fun onSuccess(data: MutableList<SpotRecommend>?) {
                listener.onSuccess(data)
                Handler().postDelayed({ querySpotRecommendRemote(spotId, pageNum, pageSize, listener) }, 5000)
            }

            override fun onFailed(e: Exception) {
                querySpotRecommendRemote(spotId, pageNum, pageSize, listener)
            }
        })
    }

    fun querySpotRecommendRemote(spotId: String, pageNum: Int, pageSize: Int, listener: QueryDoneListener<SpotRecommend>) {
        val query = BmobQuery<SpotRecommend>()
        val spot = Spot()
        spot.objectId = spotId
        query.addWhereEqualTo("spot", spot)
                .include("spot")
                .setLimit(pageSize)
                .setSkip(pageNum * pageSize)
                .findObjects(object : FindListener<SpotRecommend>() {
                    override fun done(recommends: MutableList<SpotRecommend>?, e: BmobException?) {
                        if (e == null) {
                            if (recommends != null && recommends.isNotEmpty()) {
                                recommends.forEach { it.async().save() }
                                listener.onSuccess(recommends)
                            }
                        } else {
                            Log.e(TAG, "Query Spot recommend failed", e)
                            listener.onFailed(e)
                        }
                    }
                })
    }

    fun querySpotImageRemote(pageNum: Int, pageSize: Int, spotId: String, listener: QueryDoneListener<SpotImage>) {
        val spot = Spot()
        spot.objectId = spotId
        val innerQuery = BmobQuery<SubSpot>().addWhereEqualTo("belonging", spot)
        BmobQuery<SpotImage>()
                .setLimit(pageSize)
                .setSkip(pageNum * pageSize)
                .addWhereMatchesQuery("spot", "SubSpot", innerQuery)
                .findObjects(object : FindListener<SpotImage>() {
                    override fun done(images: MutableList<SpotImage>?, e: BmobException?) {
                        if (e == null) {
                            listener.onSuccess(images)
                        } else {
                            listener.onFailed(e)
                        }
                    }

                })
    }

    fun querySpotRemote(pageNum: Int, pageSize: Int, listener: QueryDoneListener<Spot>) {
        val query = BmobQuery<Spot>()
                .setSkip(pageNum * pageSize)
                .setLimit(pageSize)
//        val isCache = query.hasCachedResult(Spot::class.java)
//        if (isCache) {
//            query.cachePolicy = BmobQuery.CachePolicy.CACHE_ELSE_NETWORK
//        } else {
//            query.cachePolicy = BmobQuery.CachePolicy.NETWORK_ELSE_CACHE
//        }
//        query.maxCacheAge = TimeUnit.DAYS.toMillis(1)
        query.findObjects(object : FindListener<Spot>() {
            override fun done(spotList: MutableList<Spot>?, e: BmobException?) {
                if (e == null) {
                    spotList?.forEach { it.async().save() }
                    listener.onSuccess(spotList)
                } else {
                    listener.onFailed(e)
                    Log.e(TAG, "Query Spot list failed", e)
                }
            }
        })
    }

    fun querySpotRemote(spotId: String, listener: QueryDoneListener<Spot>) {
        BmobQuery<Spot>()
                .getObject(spotId, object : QueryListener<Spot>() {
                    override fun done(spot: Spot?, e: BmobException?) {
                        if (e == null) {
                            spot?.async()?.save()
                            listener.onSuccess(mutableListOf(spot!!))
                        } else {
                            listener.onFailed(Exception("Query Remote Data Failed"))
                        }
                    }
                })
    }

    override fun spotClickTimesPlus(spotId: String, plusTimes: Int) {
        super.spotClickTimesPlus(spotId, plusTimes)
        val spot = Spot()
        spot.objectId = spotId
        spot.increment("clickTimes", plusTimes)
        spot.update()
    }

    override fun queryImageLoading(listener: QueryDoneListener<ImageLoading>) {
        super.queryImageLoading(object : QueryDoneListener<ImageLoading> {
            override fun onSuccess(data: MutableList<ImageLoading>?) {
                listener.onSuccess(data)
                Handler().postDelayed({ queryImageLoadingRemote(null) }, 5000)
            }

            override fun onFailed(e: Exception) {
                queryImageLoadingRemote(listener)
            }

        })
    }

    fun queryImageLoadingRemote(listener: QueryDoneListener<ImageLoading>?) {
        BmobQuery<ImageLoading>()
                .findObjects(object : FindListener<ImageLoading>() {
                    override fun done(images: MutableList<ImageLoading>?, e: BmobException?) {
                        if (e == null) {
                            images?.forEach { it.async().save() }
                            listener?.onSuccess(images)
                        } else {
                            listener?.onFailed(Exception("Query Remote Image Loading failed, message: ${e.message}"))
                        }
                    }
                })
    }

    override fun queryWall(pageNum: Int, pageSize: Int, listener: QueryDoneListener<Wall>) {
        super.queryWall(pageNum, pageSize, object : QueryDoneListener<Wall> {
            override fun onFailed(e: Exception) {
                queryWallRemote(pageNum, pageSize, listener)
            }

            override fun onSuccess(data: MutableList<Wall>?) {
                listener.onSuccess(data)
                Handler().postDelayed({
                    queryWallRemote(pageNum, pageSize, listener)
                }, 5000)
            }

        })
    }

    fun queryWallRemote(pageNum: Int, pageSize: Int, listener: QueryDoneListener<Wall>) {
        BmobQuery<Wall>()
                .setSkip(pageNum * pageSize)
                .setLimit(pageSize)
                .findObjects(object : FindListener<Wall>() {
                    override fun done(walls: MutableList<Wall>?, e: BmobException?) {
                        if (e == null) {
                            walls?.forEach { it.async().save() }
                            listener.onSuccess(walls)
                        } else {
                            listener.onFailed(Exception("Query Remote Wall data failed, message: ${e.message}"))
                        }
                    }
                })
    }

    fun queryWallRemote(wallId: String, listener: QueryDoneListener<Wall?>) {
        BmobQuery<Wall>()
                .include("user.username")
                .getObject(wallId, object : QueryListener<Wall>() {
                    override fun done(wall: Wall?, e: BmobException?) {
                        if (e == null) {
                            listener.onSuccess(mutableListOf(wall))
                        } else {
                            listener.onFailed(e)
                        }
                    }
                })
    }

    override fun queryUser(id: String, listener: QueryDoneListener<FreeWillUser>) {
        super.queryUser(id, object : QueryDoneListener<FreeWillUser> {
            override fun onSuccess(data: MutableList<FreeWillUser>?) {
                listener.onSuccess(data)
                Handler().postDelayed({ queryUserRemote(id, listener) }, 1000)
            }

            override fun onFailed(e: Exception) {
                queryUserRemote(id, listener)
            }

        })
    }

    fun queryUserRemote(id: String, listener: QueryDoneListener<FreeWillUser>) {
        BmobQuery<FreeWillUser>()
                .getObject(id, object : QueryListener<FreeWillUser>() {
                    override fun done(user: FreeWillUser?, e: BmobException?) {
                        if (e == null) {
                            user!!.async().save()
                            listener.onSuccess(mutableListOf(user))
                        } else {
                            listener.onFailed(e)
                        }
                    }
                })
    }

    fun queryMyWallLike(wallId: String, listener: QueryDoneListener<WallLike>) {
        val wall = Wall()
        wall.objectId = wallId
        BmobQuery<WallLike>()
                .addWhereEqualTo("user", BmobUser.getCurrentUser(FreeWillUser::class.java))
                .addWhereEqualTo("wall", wall)
                .findObjects(object : FindListener<WallLike>() {
                    override fun done(likes: MutableList<WallLike>?, e: BmobException?) {
                        if (e == null && likes!!.isNotEmpty()) {
                            listener.onSuccess(likes)
                        } else if (e != null) {
                            listener.onFailed(e)
                        }
                    }

                })
    }

    fun queryWallComment(wallId: String, pageNum: Int, pageSize: Int, listener: QueryDoneListener<WallComment>) {
        val wall = Wall()
        wall.objectId = wallId
        BmobQuery<WallComment>()
                .addWhereEqualTo("wall", BmobPointer(wall))
                .include("owner")
                .setLimit(pageSize)
                .setSkip(pageNum * pageSize)
                .findObjects(object : FindListener<WallComment>() {
                    override fun done(comments: MutableList<WallComment>, e: BmobException?) {
                        if (e == null) {
                            listener.onSuccess(comments)
                        } else {
                            listener.onFailed(e)
                        }
                    }
                })
    }

    fun queryWallLikes(wallId: String, pageNum: Int, pageSize: Int, listener: QueryDoneListener<WallLike>) {
        val wall = Wall()
        wall.objectId = wallId
        BmobQuery<WallLike>()
                .addWhereEqualTo("wall", BmobPointer(wall))
                .include("user")
                .setLimit(pageSize)
                .setSkip(pageNum * pageSize)
                .findObjects(object : FindListener<WallLike>() {
                    override fun done(likes: MutableList<WallLike>?, e: BmobException?) {
                        if (e == null) {
                            listener.onSuccess(likes)
                        } else {
                            listener.onFailed(e)
                        }
                    }

                })
    }

    fun queryArticle(pageNum: Int, pageSize: Int, listener: QueryDoneListener<_Article>) {
        BmobQuery<_Article>()
                .setLimit(pageSize)
                .setSkip(pageNum * pageSize)
                .order("-createdAt")
                .findObjects(object : FindListener<_Article>() {
                    override fun done(articles: MutableList<_Article>?, e: BmobException?) {
                        if (e == null) {
                            listener.onSuccess(articles)
                        } else {
                            listener.onFailed(e)
                        }
                    }

                })
    }

    fun queryMyComments(pageNum: Int, pageSize: Int, listener: QueryDoneListener<WallComment>) {
        val me = BmobUser.getCurrentUser(FreeWillUser::class.java) as FreeWillUser
        val myWallQuery = BmobQuery<Wall>()
                .addWhereEqualTo("user", me)
        BmobQuery<WallComment>()
                .setLimit(pageSize)
                .setSkip(pageNum * pageSize)
                .order("-createdAt")
                .addWhereMatchesQuery("wall", "Wall", myWallQuery)
                .include("owner")
                .findObjects(object : FindListener<WallComment>() {
                    override fun done(comments: MutableList<WallComment>?, e: BmobException?) {
                        if (e == null) {
                            listener.onSuccess(comments)
                        } else {
                            listener.onFailed(e)
                        }
                    }

                })
    }

    fun queryMyWalls(pageNum: Int, pageSize: Int, listener: QueryDoneListener<Wall>) {
        val me = BmobUser.getCurrentUser(FreeWillUser::class.java) as FreeWillUser
        BmobQuery<Wall>()
                .setLimit(pageSize)
                .setSkip(pageNum * pageSize)
                .addWhereEqualTo("user", me)
                .findObjects(object : FindListener<Wall>() {
                    override fun done(walls: MutableList<Wall>?, e: BmobException?) {
                        if (e == null) {
                            listener.onSuccess(walls)
                        } else {
                            listener.onFailed(e)
                        }
                    }

                })
    }

    fun queryMyLikes(pageNum: Int, pageSize: Int, listener: QueryDoneListener<WallLike>) {
        val me = BmobUser.getCurrentUser(FreeWillUser::class.java) as FreeWillUser
        val myWallQuery = BmobQuery<Wall>()
                .addWhereEqualTo("user", me)
        BmobQuery<WallLike>()
                .setLimit(pageSize)
                .setSkip(pageNum * pageSize)
                .order("-createdAt")
                .addWhereMatchesQuery("wall", "Wall", myWallQuery)
                .include("user")
                .findObjects(object : FindListener<WallLike>() {
                    override fun done(comments: MutableList<WallLike>?, e: BmobException?) {
                        if (e == null) {
                            listener.onSuccess(comments)
                        } else {
                            listener.onFailed(e)
                        }
                    }

                })
    }

    fun queryAllSpotImages(pageNum: Int, pageSize: Int, listener: QueryDoneListener<SpotImage>) {
        BmobQuery<SpotImage>()
                .setLimit(pageSize)
                .setSkip(pageNum * pageSize)
                .order("-createdAt")
                .findObjects(object : FindListener<SpotImage>() {
                    override fun done(images: MutableList<SpotImage>?, e: BmobException?) {
                        if (e == null) {
                            listener.onSuccess(images)
                        } else {
                            listener.onFailed(e)
                        }
                    }

                })
    }

    fun querySubSpot(spotId: String, listener: QueryDoneListener<SubSpot>) {
        val spot = Spot()
        spot.objectId = spotId
        BmobQuery<SubSpot>()
                .addWhereEqualTo("belonging", spot)
                .findObjects(object : FindListener<SubSpot>() {
                    override fun done(spots: MutableList<SubSpot>?, e: BmobException?) {
                        if (e == null) {
                            listener.onSuccess(spots)
                        } else {
                            listener.onFailed(e)
                        }
                    }

                })

    }

    fun querySpotRecommendImage(recommendId: String, pageNum: Int, pageSize: Int, listener: QueryDoneListener<SpotRecommendImage>) {
        val recommend = SpotRecommend()
        recommend.objectId = recommendId
        BmobQuery<SpotRecommendImage>()
                .setLimit(pageSize)
                .setSkip(pageNum * pageSize)
                .addWhereEqualTo("spotRecommend", recommend)
                .findObjects(object : FindListener<SpotRecommendImage>() {
                    override fun done(images: MutableList<SpotRecommendImage>?, e: BmobException?) {
                        if (e == null) {
                            listener.onSuccess(images)
                        } else {
                            listener.onFailed(e)
                        }
                    }

                })
    }
}

object RemoteDataQueryObj : RemoteData()