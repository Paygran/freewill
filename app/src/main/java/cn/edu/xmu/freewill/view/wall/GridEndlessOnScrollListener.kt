package cn.edu.xmu.freewill.view.wall

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import cn.edu.xmu.freewill.view.common.EndlessOnScrollListener

/**
 * Created by eason on 2018/1/2.
 */
class GridEndlessOnScrollListener(private val layoutManager: StaggeredGridLayoutManager) : EndlessOnScrollListener() {
    override fun isLoading(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>): Boolean {
        val wallAdapter = adapter as WallAdapter
        return wallAdapter.isLoading
    }

    override fun onLoadMore(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
        val wallAdapter = adapter as WallAdapter
        wallAdapter.loadMore()
    }

    override fun getItemCount(): Int = layoutManager.itemCount

    override fun getLastVisibleItemPosition(): Int = layoutManager.findLastVisibleItemPositions(null).last { it != RecyclerView.NO_POSITION }
}