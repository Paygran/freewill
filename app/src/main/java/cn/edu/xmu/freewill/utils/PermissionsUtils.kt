package cn.edu.xmu.freewill.utils

import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import cn.edu.xmu.freewill.R

/**
 * Created by eason on 2017/9/28.
 */
@RequiresApi(Build.VERSION_CODES.M)
object PermissionsUtils {
    const val REQ_CODE_CAMERA = 100
    const val REQ_CODE_LOCATION = 101
    const val REQ_CODE_FILE = 102
    const val REQ_CODE_PHONE = 103

    fun showDialog(context: Context, msgId: Int, onOk: () -> Unit) {
        val dialogBuilder = AlertDialog.Builder(context)
        dialogBuilder.setMessage(msgId)
                .setTitle(R.string.title_dialog_permission_rtionale)
                .setPositiveButton(R.string.action_ok) { dialog, _ ->
                    onOk.invoke()
                    dialog.dismiss()
                }
                .setNegativeButton(R.string.action_cancel) { dialog, _ ->
                    dialog.cancel()
                }
                .show()
    }

    fun checkAndRequestPermission(activity: Activity,
                                  permission: String,
                                  rationaleId: Int,
                                  requestCode: Int,
                                  onGrant: () -> Unit) =
            if (ContextCompat.checkSelfPermission(activity, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                    showDialog(activity, rationaleId) {
                        ActivityCompat.requestPermissions(activity, arrayOf(permission), requestCode)
                    }
                } else {
                    ActivityCompat.requestPermissions(activity, arrayOf(permission), requestCode)
                }
            } else {
                onGrant.invoke()
            }
}