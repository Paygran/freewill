package cn.edu.xmu.freewill.view.home

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.view.main.MainActivity
import cn.edu.xmu.freewill.view.notification.NotificationFragment
import cn.edu.xmu.freewill.view.spot.SpotListFragment
import cn.edu.xmu.freewill.view.wall.WallFragment
import kotlinx.android.synthetic.main.fragment_homepage.*

/**
 * Created by eason on 2018/3/9.
 */
class HomepageFragment : Fragment() {
    private var mListener: OnSlideListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_homepage, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        contentPager.adapter = ContentPagerAdapter()
        contentPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> bottomView.selectedItemId = R.id.navigation_spot_list
                    1 -> bottomView.selectedItemId = R.id.navigation_wall
                    2 -> bottomView.selectedItemId = R.id.navigation_notifications
                }
                mListener?.onSlide(position)
            }

        })
        bottomView.onSpotListClick = {
            contentPager.currentItem = 0
        }
        bottomView.onWallClick = {
            contentPager.currentItem = 1
        }
        bottomView.onNotificationClick = {
            contentPager.currentItem = 2
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnSlideListener) {
            mListener = context
        } else {
            throw RuntimeException("Activity doesn't implement ${OnSlideListener::class.simpleName} interface")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onResume() {
        val a = activity as MainActivity
        a.supportActionBar?.setTitle(R.string.app_name)
        mListener?.onSlide(contentPager.currentItem)
        super.onResume()
    }

    private inner class ContentPagerAdapter : FragmentStatePagerAdapter(activity?.supportFragmentManager) {
        override fun getItem(position: Int): Fragment {
            when (position) {
                0 -> return SpotListFragment.newInstance()
                1 -> return WallFragment.newInstance()
                2 -> return NotificationFragment.newInstance()
            }
            return SpotListFragment.newInstance()
        }

        override fun getCount(): Int = 3

    }

    interface OnSlideListener {
        fun onSlide(position: Int)
    }

    companion object {
        fun newInstance(): HomepageFragment = HomepageFragment()
    }
}