package cn.edu.xmu.freewill.view.common

import android.support.v7.widget.RecyclerView


/**
 * Created by paygran on 17-8-24.
 */
abstract class EndlessOnScrollListener : RecyclerView.OnScrollListener() {
    private var visibleThreshold = 2
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0

    abstract fun onLoadMore(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>)

    abstract fun getItemCount(): Int

    abstract fun getLastVisibleItemPosition(): Int

    abstract fun isLoading(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>): Boolean

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dy <= 0) return
        totalItemCount = getItemCount()
        lastVisibleItem = getLastVisibleItemPosition()
//        if (loading) {
//            if (totalItemCount > previousTotal) {
//                loading = false
//                previousTotal = totalItemCount
//            }
//        }
        if (!isLoading(recyclerView.adapter) &&
                totalItemCount <= (lastVisibleItem + visibleThreshold)) {
            onLoadMore(recyclerView.adapter)
        }
    }

}