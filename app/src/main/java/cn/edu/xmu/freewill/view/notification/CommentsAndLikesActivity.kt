package cn.edu.xmu.freewill.view.notification

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import cn.bmob.v3.BmobUser
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.view.common.Refreshable
import cn.edu.xmu.freewill.view.notification.comment.MyCommentsFragment
import cn.edu.xmu.freewill.view.notification.like.MyLikesFragment
import cn.edu.xmu.freewill.view.user.ConfirmDialog
import kotlinx.android.synthetic.main.activity_comments_and_likes.*
import kotlinx.android.synthetic.main.content_comments_and_likes.*

class CommentsAndLikesActivity : AppCompatActivity() {
    private var mInit = INIT_COMMENTS

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comments_and_likes)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mInit = intent.getIntExtra(ARG_INIT, INIT_COMMENTS)
    }

    override fun onResume() {
        super.onResume()
        checkUser()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item!!.itemId) {
        android.R.id.home -> {
            super.onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            ConfirmDialog.REQ_SIGN_UP -> ConfirmDialog.signUp(this)
        }
    }

    private fun checkUser() {
        if (BmobUser.getCurrentUser() == null) {
            ConfirmDialog.show(this, { finish() })
        } else {
            initViewPager()
        }
    }

    private fun initViewPager() {
        val adapter = MyNotificationPagerAdapter()
        myNotificationViewPager.adapter = adapter
        tabs.setupWithViewPager(myNotificationViewPager)
        myNotificationViewPager.setCurrentItem(mInit, false)
        swipeRefreshLayout.setOnRefreshListener {
            val refreshable = adapter.getItem(myNotificationViewPager.currentItem) as Refreshable
            refreshable.refresh {
                swipeRefreshLayout.isRefreshing = false
            }
        }
    }

    inner class MyNotificationPagerAdapter : FragmentStatePagerAdapter(supportFragmentManager) {
        override fun getCount(): Int = 2

        override fun getItem(position: Int): Fragment = if (position == INIT_COMMENTS) {
            MyCommentsFragment.newInstance()
        } else {
            MyLikesFragment.newInstance()
        }

        override fun getPageTitle(position: Int): CharSequence = if (position == INIT_COMMENTS) {
            this@CommentsAndLikesActivity.getString(R.string.title_my_comment)
        } else {
            this@CommentsAndLikesActivity.getString(R.string.title_my_likes)
        }
    }

    companion object {
        val ARG_INIT = "init"
        val INIT_COMMENTS = 0
        val INIT_LIKES = 1
    }
}
