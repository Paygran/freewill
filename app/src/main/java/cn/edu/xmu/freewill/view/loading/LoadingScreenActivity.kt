package cn.edu.xmu.freewill.view.loading

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.ImageLoading
import cn.edu.xmu.freewill.data.QueryDoneListener
import cn.edu.xmu.freewill.data.RemoteDataQueryObj
import cn.edu.xmu.freewill.view.main.MainActivity

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class LoadingScreenActivity : AppCompatActivity() {

    private lateinit var mContentView: ViewPager
    private var mVisible: Boolean = false
    private lateinit var mLoadingImages: List<ImageLoading>

    private val mQueryLoadingImageListener = object : QueryDoneListener<ImageLoading> {
        override fun onSuccess(data: MutableList<ImageLoading>?) {
            if (data != null) {
                mLoadingImages = data
                mContentView.adapter = LoadingScreenPageAdapter(supportFragmentManager)
            } else {
                onFailed(Exception("Empty Loading Image List"))
            }
        }

        override fun onFailed(e: Exception) {
            Log.e(TAG, e.message)
            val intent = Intent(this@LoadingScreenActivity, MainActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        firstOpenCheck()

        setContentView(R.layout.activity_loading_screen)

        mVisible = true
        mContentView = findViewById<ViewPager>(R.id.fullscreen_content)
        RemoteDataQueryObj.queryImageLoading(mQueryLoadingImageListener)

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener { toggle() }

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
//        Handler().postDelayed({
//            val mainIntent = Intent(this@LoadingScreenActivity, MainActivity::class.java)
//            startActivity(mainIntent)
//            finish()
//        }, 3000)
    }

    @SuppressLint("CommitPrefEdits")
    private fun firstOpenCheck() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        val isFirstOpen = sharedPref.getBoolean(getString(R.string.saved_first_open_key), true)
        if (isFirstOpen) {
            with(sharedPref.edit()) {
                putBoolean(getString(R.string.saved_first_open_key), false)
                apply()
            }
        } else {
            val intent = Intent(this@LoadingScreenActivity, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100)
    }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */

    private fun toggle() {
        if (mVisible) {
            hide()
        } else {
            show()
        }
    }

    private fun hide() {
        // Hide UI first
        val actionBar = supportActionBar
        actionBar?.hide()
        mVisible = false

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable)
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    private val mHidePart2Runnable = Runnable {
        // Delayed removal of status and navigation bar

        // Note that some of these constants are new as of API 16 (Jelly Bean)
        // and API 19 (KitKat). It is safe to use them, as they are inlined
        // at compile-time and do nothing on earlier devices.
        mContentView.systemUiVisibility = View.SYSTEM_UI_FLAG_LOW_PROFILE or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    }

    @SuppressLint("InlinedApi")
    private fun show() {
        // Show the system bar
        mContentView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        mVisible = true

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable)
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    private val mShowPart2Runnable = Runnable {
        // Delayed display of UI elements
        val actionBar = supportActionBar
        actionBar?.show()
    }

    private val mHideHandler = Handler()
    private val mHideRunnable = Runnable { hide() }


    private fun delayedHide(delayMillis: Int) {
        mHideHandler.removeCallbacks(mHideRunnable)
        mHideHandler.postDelayed(mHideRunnable, delayMillis.toLong())
    }

    companion object {
        private val TAG = LoadingScreenActivity::class.simpleName

        private const val UI_ANIMATION_DELAY = 300

        private const val LOADING_SCREEN_SIZE = 3
    }

    private inner class LoadingScreenPageAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            val image = mLoadingImages[position]
            if (mLoadingImages.lastIndex == position) {
                return LoadingScreenFragment.newInstance(image.objectId, image.image!!, true)
            }
            return LoadingScreenFragment.newInstance(image.objectId, image.image!!, false)
        }

        override fun getCount(): Int = if (mLoadingImages.size < LOADING_SCREEN_SIZE) mLoadingImages.size else LOADING_SCREEN_SIZE

    }
}
