package cn.edu.xmu.freewill.view.notification

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import cn.bmob.v3.datatype.BmobFile
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data._Article
import cn.edu.xmu.freewill.utils.loadThumbnail
import com.makeramen.roundedimageview.RoundedImageView

/**
 * Created by eason on 2018/1/12.
 */
abstract class NotificationAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mNotificationList = mutableListOf<_Article>()

    private var mCurrPage = -1

    private var isLoading = false
        set(value) {
            if (value) addLoadingView()
            else removeLoadingView()
            field = value
        }

    abstract fun loadData(pageNum: Int)

    override fun getItemCount(): Int =
            mNotificationList.size + 2

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (position) {
            POSITION_COMMENTS -> {
                holder as NotificationViewHolder
                holder.avatarView.setImageResource(R.drawable.ic_textsms)
                holder.titleView.setText(R.string.title_my_comment)
                holder.itemView.setOnClickListener {
                    val intent = Intent(holder.itemView.context, CommentsAndLikesActivity::class.java)
                    intent.putExtra(CommentsAndLikesActivity.ARG_INIT, CommentsAndLikesActivity.INIT_COMMENTS)
                    holder.itemView.context.startActivity(intent)
                }
            }
            POSITION_LIKES -> {
                holder as NotificationViewHolder
                holder.avatarView.setImageResource(R.drawable.ic_favorite_white)
                holder.titleView.setText(R.string.title_my_likes)
                holder.itemView.setOnClickListener {
                    val intent = Intent(holder.itemView.context, CommentsAndLikesActivity::class.java)
                    intent.putExtra(CommentsAndLikesActivity.ARG_INIT, CommentsAndLikesActivity.INIT_LIKES)
                    holder.itemView.context.startActivity(intent)
                }
            }
            else -> {
                holder as NotificationViewHolder
                val article = mNotificationList[position - 2]
                if (article.cover != null) {
                    val coverFile = BmobFile()
                    coverFile.url = article.cover
                    coverFile.loadThumbnail(holder.itemView.context, article.objectId, {
                        holder.avatarView.setImageURI(Uri.fromFile(it))
                    })
                }
                holder.titleView.text = article.title
                holder.timestampView.text = article.createdAt
                holder.itemView.setOnClickListener {
                    val intent = Intent(holder.itemView.context, ArticleDetailActivity::class.java)
                    intent.putExtra(ArticleDetailActivity.ARG_URL, article.url)
                    intent.putExtra(ArticleDetailActivity.ARG_TITLE, article.title)
                    holder.itemView.context.startActivity(intent)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int = when (position) {
        POSITION_COMMENTS, POSITION_LIKES -> position
        else -> VIEW_TYPE_ARTICLES
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_notification, parent, false)
        val holder = NotificationViewHolder(v)
        when (viewType) {
            POSITION_COMMENTS, POSITION_LIKES -> {
                holder.avatarView.scaleType = ImageView.ScaleType.CENTER
                val res = holder.itemView.context.resources
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    holder.avatarView.setBackgroundColor(res.getColor(R.color.colorPrimary, null))
                } else {
                    holder.avatarView.setBackgroundColor(res.getColor(R.color.colorPrimary))
                }
                holder.timestampView.visibility = View.GONE
            }
        }
        return holder
    }

    fun loadMore() {
        isLoading = true
        loadData(mCurrPage + 1)
    }

    protected fun onDoneQuery(pageNum: Int, articles: MutableList<_Article>) {
        isLoading = false
        if (articles.isNotEmpty()) {
            mCurrPage = pageNum
            val start = mNotificationList.lastIndex + 1
            mNotificationList.addAll(articles)
            notifyItemRangeInserted(start, articles.size)
        }
    }

    protected fun onQueryFailed() {
        isLoading = false
    }

    private fun addLoadingView() {

    }

    private fun removeLoadingView() {

    }

    inner class NotificationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            ButterKnife.bind(this, itemView)
        }

        @BindView(R.id.avatar)
        lateinit var avatarView: RoundedImageView

        @BindView(R.id.titleView)
        lateinit var titleView: TextView

        @BindView(R.id.timestampView)
        lateinit var timestampView: TextView
    }

    companion object {
        private const val POSITION_COMMENTS = 0
        private const val POSITION_LIKES = 1
        private const val VIEW_TYPE_ARTICLES = 2
    }
}