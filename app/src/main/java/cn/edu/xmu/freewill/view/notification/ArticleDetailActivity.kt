package cn.edu.xmu.freewill.view.notification

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import cn.edu.xmu.freewill.R
import kotlinx.android.synthetic.main.activity_article_detail.*
import kotlinx.android.synthetic.main.content_article_detail.*

class ArticleDetailActivity : AppCompatActivity() {

    private var mUrl: String = ""
    private var mTitle: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mTitle = intent.getStringExtra(ARG_TITLE)
        supportActionBar?.title = mTitle
        mUrl = intent.getStringExtra(ARG_URL)
        webView.loadUrl(mUrl)
    }

    override fun onDestroy() {
        webView.clearHistory()
        webView.destroy()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item!!.itemId) {
        android.R.id.home -> {
            super.onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    companion object {
        val ARG_URL = "url"
        val ARG_TITLE = "title"
    }

}
