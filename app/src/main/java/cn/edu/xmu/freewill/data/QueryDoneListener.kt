package cn.edu.xmu.freewill.data

/**
 * Created by paygran on 17-8-22.
 */

interface QueryDoneListener<T> {
    fun onSuccess(data: MutableList<T>?)
    fun onFailed(e: Exception)
}