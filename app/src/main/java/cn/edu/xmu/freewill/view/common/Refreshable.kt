package cn.edu.xmu.freewill.view.common

/**
 * Created by eason on 2018/1/8.
 */
interface Refreshable {
    fun refresh(doneRefresh: () -> Unit)
}