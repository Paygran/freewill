package cn.edu.xmu.freewill

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import cn.bmob.v3.Bmob
import com.iflytek.cloud.SpeechConstant
import com.iflytek.cloud.SpeechUtility
import com.raizlabs.android.dbflow.config.FlowConfig
import com.raizlabs.android.dbflow.config.FlowManager

/**
 * Created by paygran on 17-8-21.
 */
class FreewillApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Bmob.initialize(this, "4005ab86a7f26802545df8bed5118472")
        FlowManager.init(FlowConfig.Builder(this).build())
        SpeechUtility.createUtility(this, "${SpeechConstant.APPID}=59b37434")
    }

    override fun onTerminate() {
        super.onTerminate()
        FlowManager.destroy()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}