package cn.edu.xmu.freewill.utils

import android.content.Context
import android.util.Log
import com.amap.api.location.AMapLocation
import com.amap.api.location.AMapLocationClient
import com.amap.api.location.AMapLocationClientOption
import java.util.concurrent.TimeUnit

/**
 * Created by paygran on 17-8-23.
 */
object MyLocation {
    internal var myLocation: AMapLocation? = null
    internal var locationTime: Long = 0
}

fun getMyLocation(ctx: Context, operation: (AMapLocation) -> Unit) {
    if (System.currentTimeMillis() - MyLocation.locationTime < TimeUnit.MINUTES.toMillis(5)) {
        operation.invoke(MyLocation.myLocation!!)
        return
    }
    val mLocationClient = AMapLocationClient(ctx.applicationContext)
    val mLocationOption = AMapLocationClientOption()
    mLocationOption.locationMode = AMapLocationClientOption.AMapLocationMode.Hight_Accuracy
    mLocationOption.isOnceLocationLatest = true
    mLocationClient.setLocationOption(mLocationOption)
    mLocationClient.setLocationListener { location ->
        if (location != null) {
            if (location.errorCode == 0) {
                MyLocation.myLocation = location
                MyLocation.locationTime = System.currentTimeMillis()
                operation.invoke(location)
            } else {
                Log.e("Location Error", "ErrorCode:${location.errorCode}, errInfo:${location.errorInfo}")
            }
        }
    }
    mLocationClient.startLocation()
}