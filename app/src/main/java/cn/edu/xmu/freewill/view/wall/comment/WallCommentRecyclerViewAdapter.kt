package cn.edu.xmu.freewill.view.wall.comment

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.support.v7.widget.RecyclerView
import android.util.LruCache
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import cn.bmob.v3.datatype.BmobFile
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.WallComment
import cn.edu.xmu.freewill.utils.loadThumbnail
import com.makeramen.roundedimageview.RoundedImageView


abstract class WallCommentRecyclerViewAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mWallComments = mutableListOf<WallComment?>()
    private var mCurrPage = -1
    protected var pageSize = 20
    var isLoading = false
        set(value) {
            if (value) addLoadingView()
            else removeLoadingView()
            field = value
        }

    private val mMemoryCache: LruCache<String, Bitmap>

    init {
        val maxMemory = Runtime.getRuntime().maxMemory() / 1024
        val cacheSize = maxMemory / 8
        mMemoryCache = object : LruCache<String, Bitmap>(cacheSize.toInt()) {
            override fun sizeOf(key: String?, bitmap: Bitmap?): Int =
                    if (bitmap == null) 0 else bitmap.byteCount / 1024
        }
    }

    abstract fun loadData(pageNum: Int = mCurrPage)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            when (viewType) {
                VIEW_TYPE_LOADING -> {
                    val view = LayoutInflater.from(parent.context)
                            .inflate(R.layout.item_loading, parent, false)
                    LoadingViewHolder(view)
                }
                else -> {
                    val view = LayoutInflater.from(parent.context)
                            .inflate(R.layout.item_wall_comment, parent, false)
                    ItemViewHolder(view)
                }
            }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val comment = mWallComments[position] ?: return
        holder as ItemViewHolder
        holder.commentView.text = comment.comment
        holder.usernameView.text = comment.owner!!.username
        holder.timestampView.text = comment.createdAt
        val avatar = comment.owner?.avatar
        if (avatar != null) {
            val avatarImg = mMemoryCache[comment.owner?.objectId]
            if (avatarImg != null) {
                holder.avatarView.setImageBitmap(avatarImg)
            } else {
                val thumbnail = BmobFile()
                thumbnail.url = avatar.url + "!/fwfh/100x100/format/png"
                thumbnail.loadThumbnail(holder.mView.context, comment.owner!!.objectId, {
                    Handler(Looper.myLooper()).post {
                        mMemoryCache.put(comment.owner!!.objectId, BitmapFactory.decodeFile(it.absolutePath))
                    }
                    holder.avatarView.setImageURI(Uri.fromFile(it))
                })
            }
        }
        holder.mView.setOnClickListener {

        }
    }

    fun loadMore() {
        if (isLoading) return
        isLoading = true
        loadData(mCurrPage + 1)
    }

    private fun addLoadingView() {
        mWallComments.add(null)
        notifyItemInserted(mWallComments.lastIndex)
    }

    private fun removeLoadingView() {
        if (mWallComments.isNotEmpty() && mWallComments.last() == null) {
            mWallComments.removeAt(mWallComments.lastIndex)
            notifyItemRemoved(mWallComments.lastIndex + 1)
        }
    }

    protected fun onDoneQuery(pageNum: Int, comments: MutableList<WallComment>) {
        isLoading = false
        if (pageNum <= mCurrPage) {
            mWallComments.clear()
            notifyDataSetChanged()
        }
        if (comments.isNotEmpty()) {
            mCurrPage = pageNum
            mWallComments.addAll(comments)
            notifyDataSetChanged()
        }
    }

    protected fun onQueryFailed() {
        isLoading = false
    }

    override fun getItemViewType(position: Int): Int =
            if (mWallComments[position] != null) VIEW_TYPE_ITEM else VIEW_TYPE_LOADING

    override fun getItemCount(): Int = mWallComments.size

    inner class ItemViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        @BindView(R.id.commentView)
        lateinit var commentView: TextView

        @BindView(R.id.avatarView)
        lateinit var avatarView: RoundedImageView

        @BindView(R.id.userNameView)
        lateinit var usernameView: TextView

        @BindView(R.id.timeStampView)
        lateinit var timestampView: TextView

        init {
            ButterKnife.bind(this, mView)
        }
    }

    inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            ButterKnife.bind(this, itemView)
        }

        @BindView(R.id.progressBar)
        lateinit var progress: ProgressBar
    }

    companion object {
        private val VIEW_TYPE_LOADING = 0
        private val VIEW_TYPE_ITEM = 1
    }
}
