package cn.edu.xmu.freewill.view.common

import android.support.v7.widget.LinearLayoutManager

/**
 * Created by eason on 2018/1/8.
 */
abstract class ListEndlessOnScrollListener(private val layoutManager: LinearLayoutManager) : EndlessOnScrollListener() {
    override fun getLastVisibleItemPosition(): Int = layoutManager.findLastVisibleItemPosition()
    override fun getItemCount(): Int = layoutManager.itemCount

}