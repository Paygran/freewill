package cn.edu.xmu.freewill.view

import android.content.Context
import android.support.design.widget.BottomNavigationView
import android.util.AttributeSet
import android.view.View
import cn.edu.xmu.freewill.R

/**
 * Created by paygran on 17-8-15.
 */
class BottomView(context: Context?, attrs: AttributeSet?) : BottomNavigationView(context, attrs) {

    var onSpotListClick: ((View) -> Unit)? = null
    var onNotificationClick: ((View) -> Unit)? = null
    var onWallClick: ((View) -> Unit)? = null

    init {
        this.inflateMenu(R.menu.navigation)
        //initAttrs(context, attrs)
        this.setOnNavigationItemSelectedListener OnNavigationItemSelectedListener@ { item ->
            when (item.itemId) {
                R.id.navigation_spot_list -> {
                    onSpotListClick?.invoke(this)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_notifications -> {
                    onNotificationClick?.invoke(this)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_wall -> {
                    onWallClick?.invoke(this)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }
    }

//    private fun initAttrs(context: Context?, attrs: AttributeSet?) {
//        val a = context?.theme?.obtainStyledAttributes(attrs, R.styleable.BottomView, 0, 0)
//        try {
//            val homeVisible = a!!.getBoolean(R.styleable.BottomView_home_visible, false)
//            val notificationsVisible = a.getBoolean(R.styleable.BottomView_personal_center_visible, false)
//            val spotListVisible = a.getBoolean(R.styleable.BottomView_wall_visible, false)
//
//            val itemMap = mapOf(
//                    R.id.navigation_spot_list to homeVisible,
//                    R.id.navigation_notifications to notificationsVisible,
//                    R.id.navigation_wall to spotListVisible)
//            (0 until menu.size())
//                    .map { menu.getItem(it) }
//                    .filter { !itemMap.getOrDefault(it.itemId, false) }
//                    .forEach {
//                        menu.removeItem(it.itemId)
//                    }
//            invalidate()
//            requestLayout()
//        } finally {
//            a?.recycle()
//        }
//    }
}