package cn.edu.xmu.freewill.view.spot.list

import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import cn.edu.xmu.freewill.view.spot.SpotConstants
import cn.edu.xmu.freewill.view.spot.detail.SpotDetailActivity

/**
 * Created by paygran on 17-8-21.
 */
class SpotList(context: Context?, attrs: AttributeSet?) : RecyclerView(context, attrs) {
    private val mPageSize = 20
    var spotListAdapter = SpotListAdapter(mPageSize)
    val mLinearLayoutManager = LinearLayoutManager(context)


    init {
        this.setHasFixedSize(true)
//        this.isNestedScrollingEnabled = false
//        mLinearLayoutManager.isSmoothScrollbarEnabled = true
//        mLinearLayoutManager.isAutoMeasureEnabled = true
        this.layoutManager = mLinearLayoutManager
        this.adapter = spotListAdapter
        spotListAdapter.onItemClick = { _, position, id ->
            val intent = Intent(context, SpotDetailActivity::class.java)
            intent.putExtra(SpotConstants.POSITION, position)
            intent.putExtra(SpotConstants.ID, id)
            context?.startActivity(intent)
        }
        initLoadListener()
    }

    private fun initLoadListener() {
        this.addOnScrollListener(object : EndlessOnScrollListener(mLinearLayoutManager) {
            override fun onLoadMore() {
                spotListAdapter.loadMore()
            }
        })
    }
}