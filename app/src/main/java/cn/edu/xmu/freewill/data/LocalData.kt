package cn.edu.xmu.freewill.data

import com.raizlabs.android.dbflow.kotlinextensions.*
import com.raizlabs.android.dbflow.sql.language.SQLite

/**
 * Created by paygran on 17-8-27.
 */
open class LocalData {
    open fun querySpot(pageNum: Int, pageSize: Int, listener: QueryDoneListener<Spot>) {
        (select from Spot::class
                limit pageSize
                offset pageNum * pageSize)
                .async list { _, list ->
            if (list.isNotEmpty()) {
                listener.onSuccess(list)
            } else {
                listener.onFailed(Exception("No Spot Recommend data found!"))
            }
        }

    }

    open fun querySpotImage(pageNum: Int, pageSize: Int, spotId: String, listener: QueryDoneListener<SpotImage>) {
        (select from SpotImage::class
                innerJoin SubSpot::class on (SpotImage_Table.spot eq SubSpot_Table.objectId)
                innerJoin Spot::class on (SubSpot_Table.belonging eq Spot_Table.objectId)
                where (Spot_Table.objectId eq spotId)
                limit pageSize
                offset pageNum * pageSize)
                .async list { _, list ->
            if (list.isNotEmpty()) {
                listener.onSuccess(list)
            } else {
                listener.onFailed(Exception("No Spot Recommend data found!"))
            }
        }
    }

    open fun querySpot(spotId: String, listener: QueryDoneListener<Spot>) {
        (select from Spot::class
                where (Spot_Table.objectId eq spotId))
                .async list { _, list ->
            if (list.isNotEmpty()) {
                listener.onSuccess(list)
            } else {
                listener.onFailed(Exception("No Spot Recommend data found!"))
            }
        }
    }

    open fun spotClickTimesPlus(spotId: String, plusTimes: Int) {
        SQLite.update(Spot::class.java)
                .set(Spot_Table.click_times.eq(Spot_Table.click_times.plus(plusTimes)))
                .where(Spot_Table.objectId.`is`(spotId))
                .async()
                .execute()
    }

    open fun querySpotRecommend(spotId: String, pageNum: Int, pageSize: Int, listener: QueryDoneListener<SpotRecommend>) {
        val spot = Spot()
        spot.objectId = spotId
        (select from SpotRecommend::class
                where (SpotRecommend_Table.spot eq spot)
                limit pageSize
                offset pageNum * pageSize)
                .async list { _, list ->
            if (list.isNotEmpty()) {
                listener.onSuccess(list)
            } else {
                listener.onFailed(Exception("No Spot Recommend data found for id: $spotId!"))
            }
        }
    }

    open fun queryImageLoading(listener: QueryDoneListener<ImageLoading>) {
        (select from ImageLoading::class)
                .async list { _, list ->
            if (list.isNotEmpty()) {
                listener.onSuccess(list)
            } else {
                listener.onFailed(Exception("No Loading Image found locally"))
            }
        }
    }

    open fun queryWall(pageNum: Int, pageSize: Int, listener: QueryDoneListener<Wall>) {
        (select from Wall::class
                limit pageSize
                offset pageNum * pageSize)
                .async list { _, list ->
            if (list.isNotEmpty()) {
                listener.onSuccess(list)
            } else {
                listener.onFailed(Exception("No Wall data found at page $pageNum"))
            }
        }
    }

    open fun queryUser(id: String, listener: QueryDoneListener<FreeWillUser>) {
        (select from FreeWillUser::class
                where (FreeWillUser_Table.objectId eq id))
                .async result { _, user ->
            if (user != null) {
                listener.onSuccess(mutableListOf(user))
            } else {
                listener.onFailed(Exception("Query user with id: $id failed"))
            }
        }
    }
}