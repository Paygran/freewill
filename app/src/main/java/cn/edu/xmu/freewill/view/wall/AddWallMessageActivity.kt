package cn.edu.xmu.freewill.view.wall

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ProgressBar
import cn.bmob.v3.BmobUser
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.SaveListener
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.FreeWillUser
import cn.edu.xmu.freewill.data.Wall
import kotlinx.android.synthetic.main.activity_add_wall_message.*
import kotlinx.android.synthetic.main.content_add_wall_message.*

class AddWallMessageActivity : AppCompatActivity() {
    private val tag = AddWallMessageActivity::class.simpleName

    private lateinit var progress: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_wall_message)
        setSupportActionBar(toolbar)
        progress = AlertDialog.Builder(this)
                .setView(ProgressBar(this))
                .create()
        progress.window.setLayout(400, 400)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_add_wall, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == R.id.action_share) {
            if (wallMessageView.text.trim().isNotEmpty()) {
                uploadMessage()
            }
            true
        } else super.onOptionsItemSelected(item)
    }

    private fun uploadMessage() {
        progress.show()
        wallMessageView.isEnabled = false
        val wallMessage = Wall(BmobUser.getCurrentUser(FreeWillUser::class.java), wallMessageView.text.toString().trim())
        wallMessage.save(object : SaveListener<String>() {
            override fun done(objectId: String?, e: BmobException?) {
                if (e == null) {
                    Log.i(tag, "Successful to add wall message, objectId: $objectId")
                    this@AddWallMessageActivity.finish()
                } else {
                    Log.e(tag, "Add wall message failed, error message: ${e.message}")
                    progress.cancel()
                    wallMessageView.isEnabled = true
                }
            }
        })
    }
}
