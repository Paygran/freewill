package cn.edu.xmu.freewill.view.image

import android.annotation.SuppressLint
import android.graphics.Point
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import cn.bmob.v3.datatype.BmobFile
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.utils.loadThumbnail

/**
 * Created by eason on 2017/9/18.
 */
class ImageFragment @SuppressLint("ValidFragment")
private constructor() : Fragment() {
    private lateinit var mId: String
    private lateinit var mUrl: String

    @BindView(R.id.spot_image)
    lateinit var mSpotImageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mUrl = arguments!!.getString(ARG_IMAGE_URL)
        mId = arguments!!.getString(ARG_IMAGE_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val v = inflater.inflate(R.layout.fragment_image, container, false)
        ButterKnife.bind(this, v)
        val imageFile = BmobFile(mUrl, "", mUrl)
        val windowDisplay = this@ImageFragment.activity!!.windowManager.defaultDisplay
        val size = Point()
        windowDisplay.getSize(size)
        imageFile.loadThumbnail(context!!, mId, size.x, size.x, {
            mSpotImageView.setImageURI(Uri.fromFile(it))
        })
        return v
    }

    companion object {
        private const val ARG_IMAGE_URL = "url"
        private const val ARG_IMAGE_ID = "id"

        fun newInstance(id: String, url: String): ImageFragment {
            val fragment = ImageFragment()
            val data = Bundle()
            data.putString(ARG_IMAGE_ID, id)
            data.putString(ARG_IMAGE_URL, url)
            fragment.arguments = data
            return fragment
        }
    }
}