package cn.edu.xmu.freewill.view.loading

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import cn.bmob.v3.datatype.BmobFile
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.utils.fromFileOrNetwork
import cn.edu.xmu.freewill.view.main.MainActivity

/**
 * Created by eason on 2017/9/25.
 */
class LoadingScreenFragment @SuppressLint("ValidFragment")
private constructor() : Fragment() {
    private lateinit var mImage: BmobFile
    private lateinit var mId: String
    private var mIsLast: Boolean = true

    private lateinit var mLoadingImageView: ImageView
    private lateinit var mBtnEnterApp: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mId = arguments!!.getString(ARG_ID)
        mImage = arguments!!.getSerializable(ARG_IMAGE) as BmobFile
        mIsLast = arguments!!.getBoolean(ARG_LAST)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val v = inflater.inflate(R.layout.fragment_loading_screen, container, false)
        mLoadingImageView = v.findViewById(R.id.loading_image)
        mBtnEnterApp = v.findViewById(R.id.button_enter)
        mBtnEnterApp.visibility = if (mIsLast) View.VISIBLE else View.GONE
        mBtnEnterApp.setOnClickListener {
            val intent = Intent(activity, MainActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }
        showImage()
        return v
    }

    private fun showImage() {
        mImage.fromFileOrNetwork(context!!, mId, {
            mLoadingImageView.setImageURI(Uri.fromFile(it))
        })
    }

    companion object {
        private const val ARG_IMAGE: String = "image"
        private const val ARG_ID: String = "id"
        private const val ARG_LAST: String = "last"

        fun newInstance(id: String, image: BmobFile, isLast: Boolean): LoadingScreenFragment {
            val fragment = LoadingScreenFragment()
            val args = Bundle()
            args.putSerializable(ARG_IMAGE, image)
            args.putString(ARG_ID, id)
            args.putBoolean(ARG_LAST, isLast)
            fragment.arguments = args
            return fragment
        }
    }
}