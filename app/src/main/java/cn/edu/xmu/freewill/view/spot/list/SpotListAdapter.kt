package cn.edu.xmu.freewill.view.spot.list

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.util.LruCache
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import cn.bmob.v3.datatype.BmobFile
import cn.bmob.v3.datatype.BmobGeoPoint
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.QueryDoneListener
import cn.edu.xmu.freewill.data.RemoteDataQueryObj
import cn.edu.xmu.freewill.data.Spot
import cn.edu.xmu.freewill.data.SpotImage
import cn.edu.xmu.freewill.utils.getMyLocation
import cn.edu.xmu.freewill.utils.loadThumbnail

/**
 * Created by paygran on 17-8-21.
 */
class SpotListAdapter(private val pageSize: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var currentPage = -1

    var loading = false

    var onItemClick: ((View, Int, String) -> Unit)? = null

    private val mMemoryCache: LruCache<String, Bitmap>

    private var spotList: MutableList<Spot?> = mutableListOf()

    init {
        loadMore()
        val maxMemory = Runtime.getRuntime().maxMemory() / 1024
        val cacheSize = maxMemory / 8
        mMemoryCache = object : LruCache<String, Bitmap>(cacheSize.toInt()) {
            override fun sizeOf(key: String?, bitmap: Bitmap?): Int =
                    if (bitmap == null) 0 else bitmap.byteCount / 1024
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == 0 && spotList[position] == null) {
            return
        }
        if (holder is ItemViewHolder) {
            val spot = spotList[position] ?: return
            holder.spotNameView.text = "${spot.name}"
            holder.spotIntroView.text = spot.introduction
            holder.spotViewTimesView.text = holder.itemView.context.getText(R.string.template_view_times)
                    .toString().format(formatViews(spot.clickTimes))
            holder.spotDistanceView.text = "--"
            getMyLocation(holder.itemView.context) { myLocation ->
                val distance = spot.location?.distanceInKilometersTo(BmobGeoPoint(myLocation.longitude, myLocation.latitude))
                        ?: return@getMyLocation
                val distanceText = if (distance < 1) {
                    distance.times(1000)
                    "<%.0fm".format(distance)
                } else {
                    "<%.1fkm".format(distance)
                }
                holder.spotDistanceView.text = distanceText
            }
            holder.spotImageView.setImageResource(R.drawable.ic_crop_original)
            val image = mMemoryCache[spot.objectId]
            if (image != null) {
                holder.spotImageView.setImageBitmap(image)
            } else {
                RemoteDataQueryObj.querySpotImageRemote(0, 1, spot.objectId, object : QueryDoneListener<SpotImage> {
                    override fun onSuccess(data: MutableList<SpotImage>?) {
                        if (data == null) return
                        val spotImage = data[0]
                        val thumbnail = BmobFile()
                        thumbnail.url = spotImage.image?.url + "!/fwfh/100x100/format/png"
                        spotImage.image?.loadThumbnail(holder.itemView.context, spotImage.objectId) {
                            mMemoryCache.put(spot.objectId, BitmapFactory.decodeFile(it.absolutePath))
                            holder.spotImageView.setImageURI(Uri.fromFile(it))
                        }
                    }

                    override fun onFailed(e: Exception) {
                        holder.spotImageView.setImageResource(R.drawable.ic_crop_original)
                    }
                })
            }
            holder.itemView.setOnClickListener {
                onItemClick?.invoke(it, position, spot.objectId)
                holder.spotViewTimesView.text = holder.itemView.context.getText(R.string.template_view_times)
                        .toString().format(formatViews(spot.clickTimes))
            }
        } else if (holder is LoadingViewHolder) {
            holder.progressBar.isIndeterminate = true
        }
    }

    override fun getItemCount(): Int = spotList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            VIEW_TYPE_LOADING -> {
                val v = LayoutInflater.from(parent.context)
                        .inflate(R.layout.item_loading, parent, false)
                return LoadingViewHolder(v)
            }
        }
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_spot_list, parent, false)
        return ItemViewHolder(v)
    }


    override fun getItemViewType(position: Int): Int =
            if (spotList[position] != null) VIEW_TYPE_ITEM else VIEW_TYPE_LOADING

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var spotImageView: ImageView = itemView.findViewById(R.id.spot_image_view)
        var spotNameView: TextView = itemView.findViewById(R.id.name_view)
        var spotIntroView: TextView = itemView.findViewById(R.id.intro_view)
        var spotViewTimesView: TextView = itemView.findViewById(R.id.view_times_view)
        var spotDistanceView: TextView = itemView.findViewById(R.id.distance_view)
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var progressBar: ProgressBar = itemView.findViewById(R.id.progressBar)
    }

    fun loadMore() {
        if (loading) return
        addLoadingView()
        RemoteDataQueryObj.querySpot(++currentPage, pageSize, object : QueryDoneListener<Spot> {
            override fun onFailed(e: Exception) {
                removeLoadingView()
            }

            override fun onSuccess(data: MutableList<Spot>?) {
                removeLoadingView()

                if (data != null && !data.isEmpty()) {
                    addData(data)
                } else {
                    currentPage--
                }
            }
        })
    }

    private fun removeLoadingView() {
        if (!loading) return
        loading = false
        if (!spotList.isEmpty()) {
            spotList.removeAt(spotList.lastIndex)
            notifyItemRemoved(spotList.size)
        }
    }

    private fun addLoadingView() {
        if (loading) return
        loading = true
        Handler().post {
            spotList.add(null)
            notifyItemInserted(spotList.lastIndex)
        }
    }

    private fun addData(newSpots: MutableList<Spot>) {
        spotList.addAll(newSpots)
        notifyItemRangeInserted(newSpots.size - newSpots.size, newSpots.size)
    }

    private fun formatViews(viewNum: Int): String {
        if (viewNum >= 10000) return "1W+"
        if (viewNum in 1000..9999) {
            return "${viewNum / 1000 * 1000}K+"
        }
        if (viewNum in 100..999) {
            return "${viewNum / 100 * 100}+"
        }
        if (viewNum in 10..99) {
            return "${viewNum / 10 * 10}+"
        }
        return viewNum.toString()
    }

    companion object {
        private const val VIEW_TYPE_ITEM = 0
        private const val VIEW_TYPE_LOADING = 1
    }
}