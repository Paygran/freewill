package cn.edu.xmu.freewill.view.user

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import cn.bmob.v3.BmobUser
import cn.bmob.v3.datatype.BmobFile
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.UpdateListener
import cn.bmob.v3.listener.UploadFileListener
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.FreeWillUser
import cn.edu.xmu.freewill.utils.PermissionsUtils
import cn.edu.xmu.freewill.utils.getFile
import cn.edu.xmu.freewill.utils.loadThumbnail
import cn.edu.xmu.freewill.view.main.MainActivity
import cn.edu.xmu.freewill.view.main.OnFragmentTransition
import com.makeramen.roundedimageview.RoundedImageView
import com.yalantis.ucrop.UCrop
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by eason on 2017/9/13.
 */
class UserDetailFragment : Fragment() {
    private var mListener: OnFragmentTransition? = null

    private lateinit var mAvatarView: RoundedImageView
    private lateinit var mUsernameView: TextView
    private lateinit var mPhoneView: TextView
    private lateinit var mEmailView: TextView
    private lateinit var mSignOutBtn: Button

    private var mUser = BmobUser.getCurrentUser(FreeWillUser::class.java)

    private var mCurrentCaptureImagePath: File? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val v = inflater.inflate(R.layout.fragment_user_detail, container, false)
        mAvatarView = v.findViewById(R.id.avatar_view)
        mUsernameView = v.findViewById(R.id.username)
        mPhoneView = v.findViewById(R.id.phone)
        mEmailView = v.findViewById(R.id.email)
        mSignOutBtn = v.findViewById(R.id.button_sign_out)
        mSignOutBtn.setOnClickListener {
            BmobUser.logOut()
            (activity as MainActivity).initUser()
            (activity as MainActivity).supportFragmentManager.popBackStack()
        }
        mAvatarView.setOnClickListener {
            AlertDialog.Builder(context!!)
                    .setItems(R.array.action_change_avatar) { dialog, which ->
                        if (which == 0) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                checkCamera()
                            }
                        } else if (which == 1) {
                            pickImage()
                        }
                        dialog.dismiss()
                    }
                    .show()
        }
        mUsernameView.setOnClickListener {
            Intent(context, EditProfileActivity::class.java).apply {
                this.putExtra(ARG_EDIT, REQUEST_EDIT_USERNAME)
                startActivityForResult(this, REQUEST_EDIT_USERNAME)
            }
        }
        return v
    }

    private fun pickImage() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        if (intent.resolveActivity(activity!!.packageManager) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_GET)
        }
    }

    private fun doCrop(imageUri: Uri) {
        val destinationFileName = System.currentTimeMillis().toString() + ".jpg"
        val avatarDir = File(context!!.externalCacheDir.absolutePath, "avatar")
        Thread {
            if (!avatarDir.exists()) {
                avatarDir.mkdirs()
            }
            Handler(Looper.getMainLooper()).post {
                val options = UCrop.Options()
                options.setCircleDimmedLayer(true)
                options.setShowCropFrame(false)
                UCrop.of(imageUri, Uri.fromFile(File(avatarDir, destinationFileName)))
                        .withAspectRatio(1f, 1f)
                        .withMaxResultSize(600, 600)
                        .withOptions(options)
                        .start(context!!, this)
            }
        }.start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_GET) {
                val imageUri = data?.data
//                val destinationFileName = System.currentTimeMillis().toString() + ".jpg"
//                val avatarDir = File(context!!.externalCacheDir.absolutePath, "avatar")
//                Thread {
//                    if (!avatarDir.exists()) {
//                        avatarDir.mkdirs()
//                    }
//                    Handler(Looper.getMainLooper()).post {
//                        if (imageUri != null) {
//                            val options = UCrop.Options()
//                            options.setCircleDimmedLayer(true)
//                            options.setShowCropFrame(false)
//                            UCrop.of(imageUri, Uri.fromFile(File(avatarDir, destinationFileName)))
//                                    .withAspectRatio(1f, 1f)
//                                    .withMaxResultSize(600, 600)
//                                    .withOptions(options)
//                                    .start(context!!, this)
//                        }
//                    }
//                }.start()
                if (imageUri != null) {
                    doCrop(imageUri)
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                if (data == null) return
                val resultImg = UCrop.getOutput(data)
                val imgBmobFile = BmobFile(resultImg?.getFile(context!!))
                imgBmobFile.uploadblock(object : UploadFileListener() {
                    override fun done(e: BmobException?) {
                        if (e == null) {
                            Log.i(TAG, "Upload successful! url: ${imgBmobFile.fileUrl}")
                            val updateUser = FreeWillUser()
                            updateUser.avatar = imgBmobFile
                            updateUser.update(BmobUser.getCurrentUser().objectId, object : UpdateListener() {
                                override fun done(e: BmobException?) {
                                    if (e == null) {
                                        Toast.makeText(context, "头像更换成功", Toast.LENGTH_SHORT)
                                                .show()
                                        imgBmobFile.loadThumbnail(context!!, mUser.objectId) {
                                            mAvatarView.setImageURI(Uri.fromFile(it))
                                        }
                                    } else {
                                        Log.e(TAG, "Change avatar failed: ${e.message}")
                                        Toast.makeText(context, "头像更换失败", Toast.LENGTH_LONG)
                                                .show()
                                    }
                                }

                            })
                        } else {
                            Log.e(TAG, "Upload failed: ${e.message}")
                        }
                    }

                })

            } else if (requestCode == REQUEST_IMAGE_CAPTURE) {
                Log.i(TAG, mCurrentCaptureImagePath?.absolutePath)
                mCurrentCaptureImagePath?.apply {
                    if (this.exists()) {
                        doCrop(FileProvider.getUriForFile(context!!,
                                "cn.edu.xmu.freewill.fileprovider",
                                this))
                    }
                }
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun createCaptureImageFile(): File? {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_${timeStamp}_"
        val dir = File(context!!.externalCacheDir, "captures")
        if (!dir.exists()) {
            dir.mkdirs()
        }
        val img = File.createTempFile(
                imageFileName,
                ".jpg",
                dir
        )
        mCurrentCaptureImagePath = img
        return img
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun checkCamera() {
        val permission = Manifest.permission.CAMERA
        if (activity!!.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(permission)) {
                PermissionsUtils.showDialog(context!!, R.string.message_request_camera) {
                    requestPermissions(arrayOf(permission), PermissionsUtils.REQ_CODE_CAMERA)
                }
            } else {
                requestPermissions(arrayOf(Manifest.permission.CAMERA), PermissionsUtils.REQ_CODE_CAMERA)
            }
        } else {
            openCamera()
        }
//        PermissionsUtils.checkAndRequestPermission(activity as Activity,
//                android.Manifest.permission.CAMERA,
//                R.string.message_request_camera,
//                PermissionsUtils.REQ_CODE_CAMERA) {
//            openCamera()
//        }
    }

    private fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(activity?.packageManager) != null) {
            val imageFile = createCaptureImageFile()
            if (imageFile != null) {
                val imageUri = FileProvider.getUriForFile(context!!,
                        "cn.edu.xmu.freewill.fileprovider",
                        imageFile)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PermissionsUtils.REQ_CODE_CAMERA -> {
                if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openCamera()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mUser = BmobUser.getCurrentUser(FreeWillUser::class.java)
        initData()
        mListener?.onTransition(context!!.getString(R.string.title_fragment_user_detail))
    }

    private fun initData() {
        if (mUser == null) {
            Toast.makeText(context, R.string.prompt_user_not_sign_in, Toast.LENGTH_LONG).show()
            val intent = Intent(context, LoginActivity::class.java)
            startActivity(intent)
            return
        }
        mUser.avatar?.loadThumbnail(context!!, mUser.objectId) {
            mAvatarView.setImageURI(Uri.fromFile(it))
        }
        mUsernameView.text = mUser.username
        mPhoneView.text = mUser.mobilePhoneNumber
        mEmailView.text = mUser.email
    }

//    override fun onAttach(context: Context?) {
//        super.onAttach(context)
//        val mainActivity = context as MainActivity
//        mainActivity.toolbarCollapse(false)
//    }
override fun onAttach(context: Context?) {
    super.onAttach(context)
    if (context is OnFragmentTransition) {
        mListener = context
    } else {
        throw RuntimeException("Bound activity doesn't implement interface.")
    }
}

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    companion object {
        private val TAG = UserDetailFragment::class.simpleName
        fun newInstance() = UserDetailFragment()
        const val REQUEST_IMAGE_CAPTURE = 1
        const val REQUEST_IMAGE_GET = 2
        const val REQUEST_EDIT_USERNAME = 3
        const val ARG_EDIT = "Edit Profile"
    }
}