package cn.edu.xmu.freewill.view.wall

import android.os.Build
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.Wall

/**
 * Created by eason on 2017/12/27.
 */
abstract class WallAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mOnItemClick: ((View, Int, Any) -> Unit)? = null
    private var mWallMessageDataList: MutableList<Wall?> = mutableListOf()
    private var mCurrPage = -1
    var isLoading = false
        set(value) {
            if (value) addLoadingView()
            else removeLoadingView()
            field = value
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder !is MsgViewHolder) return
        val message = mWallMessageDataList[position]
        holder.messageView.text = message?.message
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.messageView.transitionName = SHARE_ELEMENT_MSG
        }
        if (mOnItemClick != null) {
            holder.itemView.setOnClickListener {
                if (message != null) {
                    mOnItemClick?.invoke(it, position, message)
                }
            }
        }
    }

    override fun getItemCount(): Int = mWallMessageDataList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_MSG) {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_wall_message, parent, false)
            MsgViewHolder(v)
        } else {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_loading, parent, false)
            LoadingViewHolder(v)
        }
    }

    override fun getItemViewType(position: Int): Int =
            if (mWallMessageDataList[position] != null) VIEW_TYPE_MSG else VIEW_TYPE_LOADING

    abstract fun loadData(pageNum: Int = 0)

    fun loadMore() {
        if (isLoading) return
        addLoadingView()
        loadData(mCurrPage + 1)
    }

    private fun addLoadingView() {
        mWallMessageDataList.add(null)
        notifyItemInserted(mWallMessageDataList.lastIndex)
    }

    private fun removeLoadingView() {
        if (mWallMessageDataList.isNotEmpty() || mWallMessageDataList.last() == null) {
            mWallMessageDataList.removeAt(mWallMessageDataList.lastIndex)
            notifyItemRemoved(mWallMessageDataList.lastIndex + 1)
        }
    }

    protected fun onDoneQuery(pageNum: Int, data: MutableList<Wall>?) {
        removeLoadingView()
        if (pageNum <= mCurrPage) {
            mWallMessageDataList.clear()
        }
        if (data!!.isNotEmpty()) {
            mCurrPage = pageNum
            mWallMessageDataList.addAll(data)
        }
        notifyDataSetChanged()
    }

    protected fun onQueryFailed() {
        removeLoadingView()
    }

    class MsgViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            ButterKnife.bind(this, itemView)
        }

        @BindView(R.id.wall_message)
        lateinit var messageView: TextView
    }

    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            ButterKnife.bind(this, itemView)
        }

        @BindView(R.id.progressBar)
        lateinit var progressBar: ProgressBar
    }

    fun setItemClickListener(onClick: (View, Int, Any) -> Unit) {
        mOnItemClick = onClick
    }

    companion object {
        const val SHARE_ELEMENT_MSG = "message"
        private val tag = WallAdapter::class.simpleName
        private const val VIEW_TYPE_MSG = 0
        private const val VIEW_TYPE_LOADING = 1
    }
}