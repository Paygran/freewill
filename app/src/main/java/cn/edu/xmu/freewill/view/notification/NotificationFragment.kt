package cn.edu.xmu.freewill.view.notification

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.QueryDoneListener
import cn.edu.xmu.freewill.data.RemoteDataQueryObj
import cn.edu.xmu.freewill.data._Article
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_notification.*

/**
 * Created by eason on 2018/1/9.
 */
class NotificationFragment : Fragment() {
    private var mNotificationListAdapter: NotificationAdapter? = null
    private val mPageSize = 20

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            layoutInflater.inflate(R.layout.fragment_notification, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = LinearLayoutManager(view.context)
        mNotificationListAdapter = object : NotificationAdapter() {
            override fun loadData(pageNum: Int) {
                RemoteDataQueryObj.queryArticle(pageNum, mPageSize, object : QueryDoneListener<_Article> {
                    override fun onFailed(e: Exception) {
                        onQueryFailed()
                    }

                    override fun onSuccess(data: MutableList<_Article>?) {
                        if (data != null) {
                            onDoneQuery(pageNum, data)
                        }
                    }

                })
            }

        }
        notificationContentList.layoutManager = layoutManager
        notificationContentList.addItemDecoration(DividerItemDecoration(context, layoutManager.orientation))
        notificationContentList.adapter = mNotificationListAdapter
        mNotificationListAdapter?.loadMore()
    }

//    override fun onResume() {
//        super.onResume()
//        val toolbar = (activity as AppCompatActivity).supportActionBar
//        toolbar?.setTitle(R.string.title_notification)
//    }

    override fun onDestroyView() {
        super.onDestroyView()
        this.clearFindViewByIdCache()
    }

    companion object {
        private val TAG = NotificationFragment::class.simpleName
        fun newInstance(): NotificationFragment = NotificationFragment()
    }
}