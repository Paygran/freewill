package cn.edu.xmu.freewill.view.main

/**
 * Created by eason on 2018/4/1.
 */
interface OnFragmentTransition {
    fun onTransition(title: String)
}