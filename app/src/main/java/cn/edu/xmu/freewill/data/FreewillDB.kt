package cn.edu.xmu.freewill.data

import com.raizlabs.android.dbflow.annotation.Database

/**
 * Created by paygran on 17-8-25.
 */
@Database(name = FreewillDB.NAME, version = FreewillDB.VERSION, generatedClassSeparator = "_")
object FreewillDB {
    const val NAME = "FreewillDB"
    const val VERSION = 16
}