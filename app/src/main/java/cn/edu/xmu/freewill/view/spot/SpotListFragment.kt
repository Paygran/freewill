package cn.edu.xmu.freewill.view.spot

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.utils.PermissionsUtils
import kotlinx.android.synthetic.*

/**
 * Created by eason on 2017/9/12.
 */
class SpotListFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_spot_list, container, false)

    @SuppressLint("NewApi")
    override fun onResume() {
        super.onResume()
        val activity = activity as AppCompatActivity
        //activity.supportActionBar!!.setTitle(R.string.title_home)
        PermissionsUtils.checkAndRequestPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION,
                R.string.message_request_loaction, PermissionsUtils.REQ_CODE_LOCATION, {})
        PermissionsUtils.checkAndRequestPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                R.string.message_request_file, PermissionsUtils.REQ_CODE_FILE, {})
    }

    override fun onDestroy() {
        clearFindViewByIdCache()
        super.onDestroy()
    }

    companion object {
        private val TAG = SpotListFragment::class.simpleName
        fun newInstance(): SpotListFragment = SpotListFragment()
    }
}