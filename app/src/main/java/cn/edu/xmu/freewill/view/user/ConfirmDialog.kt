package cn.edu.xmu.freewill.view.user

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AlertDialog
import cn.edu.xmu.freewill.R

/**
 * Created by eason on 2018/1/16.
 */
object ConfirmDialog {
    const val REQ_LOGIN = 100
    const val REQ_SIGN_UP = 101

    fun show(activity: Activity, onDismiss: () -> Unit) {
        AlertDialog.Builder(activity)
                .setTitle(R.string.title_alert_not_login)
                .setMessage(R.string.message_alert_not_login)
                .setPositiveButton(R.string.action_sign_in, { _, _ ->
                    login(activity)
                })
                .setNeutralButton(R.string.action_sign_up, { _, _ ->
                    signUp(activity)
                })
                .setNegativeButton(R.string.action_cancel, { dialog, _ ->
                    dialog.dismiss()
                    onDismiss()
                })
                .show()
    }

    fun login(activity: Activity) {
        val intent = Intent(activity, LoginActivity::class.java)
        activity.startActivityForResult(intent, REQ_LOGIN)
    }

    fun signUp(activity: Activity) {
        val intent = Intent(activity, SignUpActivity::class.java)
        activity.startActivityForResult(intent, REQ_SIGN_UP)
    }
}