package cn.edu.xmu.freewill.view.common

/**
 * Created by eason on 2018/1/11.
 */
interface Scrollable {
    fun isOnTop(): Boolean
    fun isOnBottom(): Boolean
}