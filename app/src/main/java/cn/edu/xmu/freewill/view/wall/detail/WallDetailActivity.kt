package cn.edu.xmu.freewill.view.wall.detail

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import cn.bmob.v3.BmobUser
import cn.bmob.v3.datatype.BmobFile
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.SaveListener
import cn.bmob.v3.listener.UpdateListener
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.*
import cn.edu.xmu.freewill.utils.loadThumbnail
import cn.edu.xmu.freewill.view.common.Refreshable
import cn.edu.xmu.freewill.view.common.Scrollable
import cn.edu.xmu.freewill.view.user.ConfirmDialog
import cn.edu.xmu.freewill.view.wall.WallAdapter
import cn.edu.xmu.freewill.view.wall.comment.AddWallCommentActivity
import cn.edu.xmu.freewill.view.wall.comment.WallCommentFragment
import cn.edu.xmu.freewill.view.wall.like.LikeFragment
import kotlinx.android.synthetic.main.activity_wall_detail.*
import kotlinx.android.synthetic.main.content_wall_detail.*
import kotlinx.android.synthetic.main.fragment_user_info.*

class WallDetailActivity : AppCompatActivity() {
    private lateinit var wallMessageId: String
    private lateinit var mWall: Wall

    private lateinit var mPageAdapter: CommentLikePagerAdapter

    private var isLiked = false
    private var likeId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wall_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            wallMessageView.transitionName = WallAdapter.SHARE_ELEMENT_MSG
        }
        initHeading()
        initWall()
        setupViewPager()
    }

    private fun initHeading() {
        wallContainer.setCanScrollVerticallyDelegate {
            val scrollable = mPageAdapter.getItem(viewPager.currentItem) as Scrollable
            scrollable.isOnTop().not()
        }
        swipeRefreshLayout.setOnChildScrollUpCallback { _, _ ->
            wallContainer.canScrollVertically(-1)
        }
    }

    private fun setupViewPager() {
        mPageAdapter = CommentLikePagerAdapter(supportFragmentManager)
        viewPager.adapter = mPageAdapter
        swipeRefreshLayout.setOnRefreshListener {
            val refreshable = mPageAdapter.getItem(viewPager.currentItem) as Refreshable
            refreshable.refresh({
                swipeRefreshLayout.isRefreshing = false
            })
        }
    }


    private fun initWall() {
        wallMessageId = intent.getStringExtra(ARG_WALL_ID)
        val message = intent.getStringExtra(ARG_WALL_MSG)
        wallMessageView.text = message
        RemoteDataQueryObj.queryWallRemote(wallMessageId, object : QueryDoneListener<Wall?> {
            override fun onSuccess(data: MutableList<Wall?>?) {
                mWall = data?.get(0)!!
                if (!mWall.message.equals(message, false)) {
                    wallMessageView.text = mWall.message
                }
                timeStampView.text = mWall.createdAt
                userNameView.text = mWall.user?.username
                val thumbnailUrl = mWall.user?.avatar?.fileUrl + "!/fwfh/100x100/format/png"
                val thumbnail = BmobFile()
                thumbnail.url = thumbnailUrl
                thumbnail.loadThumbnail(this@WallDetailActivity, mWall.user?.objectId!!, {
                    avatarView.setImageURI(Uri.fromFile(it))
                })
            }

            override fun onFailed(e: Exception) {
                Log.e(TAG, e.message)
                finish()
            }

        })

        RemoteDataQueryObj.queryMyWallLike(wallMessageId, object : QueryDoneListener<WallLike> {
            override fun onFailed(e: Exception) {
                Log.e(TAG, e.message)
            }

            override fun onSuccess(data: MutableList<WallLike>?) {
                isLiked = true
                likeId = data!![0].objectId
                btnFavorite.setImageResource(R.drawable.ic_favorite)
            }

        })

        btnComment.setOnClickListener {
            if (!checkUser()) {
                ConfirmDialog.show(this, {})
                return@setOnClickListener
            }
            val intent = Intent(this@WallDetailActivity, AddWallCommentActivity::class.java)
            intent.putExtra(AddWallCommentActivity.ARG_WALL_ID, mWall.objectId)
            startActivity(intent)
        }

        btnFavorite.setOnClickListener {
            if (!checkUser()) {
                ConfirmDialog.show(this, {})
                return@setOnClickListener
            }
            if (isLiked) {
                btnFavorite.setImageResource(R.drawable.ic_favorite_border)
            } else {
                btnFavorite.setImageResource(R.drawable.ic_favorite)
            }
            setLike(!isLiked)
        }
    }

    private fun checkUser(): Boolean =
            BmobUser.getCurrentUser(FreeWillUser::class.java) != null

    private fun setLike(liked: Boolean) {
        if (liked) {
            like()
        } else {
            unlike()
        }
    }

    private fun like() {
        val wallLike = WallLike()
        wallLike.user = BmobUser.getCurrentUser(FreeWillUser::class.java)
        wallLike.wall = mWall
        wallLike.save(object : SaveListener<String>() {
            override fun done(id: String, e: BmobException?) {
                if (e == null) {
                    Log.i(TAG, "Like")
                    likeId = id
                    isLiked = true
                } else {
                    Log.e(TAG, e.message)
                    Toast.makeText(this@WallDetailActivity, "Network unavailable!", Toast.LENGTH_LONG).show()
                }
            }

        })
    }

    private fun unlike() {
        if (likeId == null || !isLiked) return
        val like = WallLike()
        like.objectId = likeId
        like.delete(object : UpdateListener() {
            override fun done(e: BmobException?) {
                if (e == null) {
                    Log.i(TAG, "Unlike")
                    isLiked = false
                    likeId = null
                } else {
                    Log.e(TAG, e.message)
                    Toast.makeText(this@WallDetailActivity, "Network unavailable", Toast.LENGTH_LONG).show()
                }
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item?.itemId) {
        android.R.id.home -> {
            this.finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    companion object {
        val TAG = WallDetailActivity::class.simpleName
        val ARG_WALL_ID = "wall id"
        val ARG_WALL_MSG = "wall message"
    }

    inner class CommentLikePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        private val fragments = mutableListOf<Fragment>(
                WallCommentFragment.newInstance(wallMessageId),
                LikeFragment.newInstance(wallMessageId)
        )

        override fun getItem(position: Int): Fragment = fragments[position]

        override fun getCount(): Int = fragments.size

        override fun getPageTitle(position: Int): CharSequence {
            if (position == 0) return "Comments"
            else return "Likes"
        }
    }
}
