package cn.edu.xmu.freewill.view.wall

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.util.Log
import android.view.*
import butterknife.BindView
import butterknife.ButterKnife
import cn.bmob.v3.BmobUser
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.QueryDoneListener
import cn.edu.xmu.freewill.data.RemoteDataQueryObj
import cn.edu.xmu.freewill.data.Wall
import cn.edu.xmu.freewill.view.user.ConfirmDialog
import cn.edu.xmu.freewill.view.wall.detail.WallDetailActivity

/**
 * Created by eason on 2017/12/26.
 */
class WallFragment @SuppressLint("ValidFragment")
private constructor() : Fragment() {
    @BindView(R.id.wall_message_list)
    lateinit var mWallMessageList: RecyclerView

    @BindView(R.id.swipe_refresh_layout)
    lateinit var mSwipeRefreshLayout: SwipeRefreshLayout

    private lateinit var mWallLayoutManager: StaggeredGridLayoutManager

    private val mWallAdapter = object : WallAdapter() {
        override fun loadData(pageNum: Int) {
            RemoteDataQueryObj.queryWallRemote(pageNum, 40, object : QueryDoneListener<Wall> {
                override fun onSuccess(data: MutableList<Wall>?) {
                    onDoneQuery(pageNum, data)
                    this@WallFragment.mSwipeRefreshLayout.isRefreshing = false
                }

                override fun onFailed(e: Exception) {
                    Log.e(tag, e.message)
                    onQueryFailed()
                    this@WallFragment.mSwipeRefreshLayout.isRefreshing = false
                }
            })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        inflater?.inflate(R.menu.menu_wall, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == R.id.action_add_wall_message) {
            //Check user and add wall message
            addWallMessage()
            true
        } else super.onOptionsItemSelected(item)
    }

    private fun addWallMessage() {
        if (BmobUser.getCurrentUser() == null) {
            //Need to login
            ConfirmDialog.show(activity!!, {})
        } else {
            val intent = Intent(context, AddWallMessageActivity::class.java)
            startActivityForResult(intent, REQ_ADD_MESSAGE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            ConfirmDialog.REQ_LOGIN -> {
                addWallMessage()
            }
            ConfirmDialog.REQ_SIGN_UP -> {
                ConfirmDialog.login(activity!!)
            }
            REQ_ADD_MESSAGE -> {
                mSwipeRefreshLayout.isRefreshing = true
                mWallAdapter.loadData(0)
            }
        }
    }

//    override fun onResume() {
//        super.onResume()
//        val activity = activity as AppCompatActivity
//        activity.supportActionBar!!.setTitle(R.string.title_wall)
//    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_wall, container, false)
        ButterKnife.bind(this, v)
        mWallMessageList.setHasFixedSize(true)
        mWallLayoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        mWallMessageList.layoutManager = mWallLayoutManager
        mWallAdapter.setItemClickListener { view, _, wallMsg ->
            wallMsg as Wall
            val intent = Intent(context, WallDetailActivity::class.java)
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity!!, view.findViewById(R.id.wall_message), WallAdapter.SHARE_ELEMENT_MSG)
            intent.putExtra(WallDetailActivity.ARG_WALL_ID, wallMsg.objectId)
            intent.putExtra(WallDetailActivity.ARG_WALL_MSG, wallMsg.message)
            context?.startActivity(intent, options.toBundle())
        }
        mWallMessageList.adapter = mWallAdapter
        mWallMessageList.addOnScrollListener(GridEndlessOnScrollListener(mWallLayoutManager))
        mWallAdapter.loadMore()

        mSwipeRefreshLayout.setOnRefreshListener {
            Log.i(TAG, "Call refresh")
            mWallAdapter.loadData(0)
        }
        return v
    }

    companion object {
        private val TAG = WallFragment::class.simpleName

        private val REQ_ADD_MESSAGE = 0
        fun newInstance(): WallFragment = WallFragment()
    }
}