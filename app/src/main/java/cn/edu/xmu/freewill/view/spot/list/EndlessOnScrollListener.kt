package cn.edu.xmu.freewill.view.spot.list

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView


/**
 * Created by paygran on 17-8-24.
 */
abstract class EndlessOnScrollListener(linearLayoutManager: LinearLayoutManager) : RecyclerView.OnScrollListener() {
    private var visibleThreshold = 2
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private val mLayoutManager: LinearLayoutManager = linearLayoutManager

    abstract fun onLoadMore()

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dy <= 0) return
        totalItemCount = mLayoutManager.itemCount
        lastVisibleItem = mLayoutManager.findLastVisibleItemPosition()
//        if (loading) {
//            if (totalItemCount > previousTotal) {
//                loading = false
//                previousTotal = totalItemCount
//            }
//        }
        val adapter = recyclerView.adapter as SpotListAdapter
        if (!adapter.loading &&
                totalItemCount <= (lastVisibleItem + visibleThreshold)) {
            onLoadMore()
        }
    }

}