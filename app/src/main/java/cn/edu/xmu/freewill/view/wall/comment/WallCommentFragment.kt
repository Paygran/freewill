package cn.edu.xmu.freewill.view.wall.comment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.QueryDoneListener
import cn.edu.xmu.freewill.data.RemoteDataQueryObj
import cn.edu.xmu.freewill.data.WallComment
import cn.edu.xmu.freewill.view.common.ListEndlessOnScrollListener
import cn.edu.xmu.freewill.view.common.Refreshable
import cn.edu.xmu.freewill.view.common.Scrollable
import kotlinx.android.synthetic.main.fragment_wall_comment_list.*

class WallCommentFragment : Fragment(), Refreshable, Scrollable {
    override fun isOnBottom(): Boolean {
        val layoutManger = commentList.layoutManager as LinearLayoutManager
        return layoutManger.itemCount == layoutManger.findLastCompletelyVisibleItemPosition()
    }

    override fun isOnTop(): Boolean {
        val layoutManger = commentList.layoutManager as LinearLayoutManager
        return layoutManger.findFirstCompletelyVisibleItemPosition() == 0
    }

    private var mWallId: String = ""

    private var mDoneRefresh: () -> Unit = {}

    private val mWallCommentListAdapter = object : WallCommentRecyclerViewAdapter() {
        override fun loadData(pageNum: Int) {
            RemoteDataQueryObj.queryWallComment(mWallId, pageNum, pageSize, object : QueryDoneListener<WallComment> {
                override fun onFailed(e: Exception) {
                    Log.e(TAG, e.message)
                    onQueryFailed()
                    mDoneRefresh()
                    mDoneRefresh = {}
                }

                override fun onSuccess(data: MutableList<WallComment>?) {
                    onDoneQuery(pageNum, data!!)
                    mDoneRefresh()
                    mDoneRefresh = {}
                }

            })
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mWallId = arguments!!.getString(ARG_WALL_ID)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_wall_comment_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        commentList.layoutManager = LinearLayoutManager(context)
        commentList.adapter = mWallCommentListAdapter
        commentList.addItemDecoration(DividerItemDecoration(context, (commentList.layoutManager as LinearLayoutManager).orientation))

        commentList.addOnScrollListener(object : ListEndlessOnScrollListener(commentList.layoutManager as LinearLayoutManager) {
            override fun isLoading(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>): Boolean =
                    mWallCommentListAdapter.isLoading

            override fun onLoadMore(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
                mWallCommentListAdapter.loadMore()
            }


        })

        mWallCommentListAdapter.loadMore()
    }

    override fun refresh(doneRefresh: () -> Unit) {
        mDoneRefresh = doneRefresh
        mWallCommentListAdapter.loadData(0)
    }

    companion object {
        private val TAG = WallCommentFragment::class.simpleName
        private val ARG_WALL_ID = "wallId"

        fun newInstance(wallId: String): WallCommentFragment {
            val fragment = WallCommentFragment()
            val args = Bundle()
            args.putString(ARG_WALL_ID, wallId)
            fragment.arguments = args
            return fragment
        }
    }
}
