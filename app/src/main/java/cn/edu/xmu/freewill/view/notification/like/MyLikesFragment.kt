package cn.edu.xmu.freewill.view.notification.like

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.QueryDoneListener
import cn.edu.xmu.freewill.data.RemoteDataQueryObj
import cn.edu.xmu.freewill.data.WallLike
import cn.edu.xmu.freewill.view.common.ListEndlessOnScrollListener
import cn.edu.xmu.freewill.view.common.Refreshable
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_wall_like.*

/**
 * Created by eason on 2018/1/14.
 */
class MyLikesFragment : Fragment(), Refreshable {
    override fun refresh(doneRefresh: () -> Unit) {
        mOnRefresh = doneRefresh
        mMyLikesAdapter.loadData(0)
    }

    private var mOnRefresh: () -> Unit = {}

    private val mMyLikesAdapter: MyLikesAdapter = object : MyLikesAdapter() {
        override fun loadData(pageNum: Int) {
            RemoteDataQueryObj.queryMyLikes(pageNum, 40, object : QueryDoneListener<WallLike> {
                override fun onFailed(e: Exception) {
                    mOnRefresh()
                    Log.e(TAG, e.message)
                    onQueryFailed()
                }

                override fun onSuccess(data: MutableList<WallLike>?) {
                    mOnRefresh()
                    if (data != null) {
                        onDoneQuery(pageNum, data)
                    } else {
                        Log.e(TAG, "Query data is null")
                        onQueryFailed()
                    }
                }

            })
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_wall_like, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManger = LinearLayoutManager(context)
        wallLikeList.addItemDecoration(DividerItemDecoration(context, layoutManger.orientation))
        wallLikeList.layoutManager = layoutManger
        wallLikeList.adapter = mMyLikesAdapter
        wallLikeList.addOnScrollListener(object : ListEndlessOnScrollListener(layoutManger) {
            override fun isLoading(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>): Boolean =
                    mMyLikesAdapter.mIsLoading

            override fun onLoadMore(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
                mMyLikesAdapter.loadMore()
            }

        })
        mMyLikesAdapter.loadMore()
    }

    override fun onDestroy() {
        this.clearFindViewByIdCache()
        super.onDestroy()
    }

    companion object {
        private val TAG = MyLikesFragment::class.simpleName
        fun newInstance(): MyLikesFragment =
                MyLikesFragment()
    }
}