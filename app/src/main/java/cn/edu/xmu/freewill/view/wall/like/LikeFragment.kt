package cn.edu.xmu.freewill.view.wall.like

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.QueryDoneListener
import cn.edu.xmu.freewill.data.RemoteDataQueryObj
import cn.edu.xmu.freewill.data.WallLike
import cn.edu.xmu.freewill.view.common.ListEndlessOnScrollListener
import cn.edu.xmu.freewill.view.common.Refreshable
import cn.edu.xmu.freewill.view.common.Scrollable

/**
 * Created by eason on 2018/1/4.
 */
class LikeFragment : Fragment(), Refreshable, Scrollable {
    override fun isOnBottom(): Boolean {
        val layoutManger = mWallLikeList.layoutManager as LinearLayoutManager
        return layoutManger.itemCount == layoutManger.findLastCompletelyVisibleItemPosition()
    }

    override fun isOnTop(): Boolean {
        val layoutManger = mWallLikeList.layoutManager as LinearLayoutManager
        return layoutManger.findFirstCompletelyVisibleItemPosition() == 0
    }

    private var mWallId: String = ""

    private var mDoneRefresh: () -> Unit = {}

    @BindView(R.id.wallLikeList)
    lateinit var mWallLikeList: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mWallId = arguments!!.getString(ARG_WALL_ID)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_wall_like, container, false)
        ButterKnife.bind(this, v)
        initWallLikes()
        return v
    }

    override fun refresh(doneRefresh: () -> Unit) {
        mDoneRefresh = doneRefresh
        (mWallLikeList.adapter as LikeAdapter).loadData(0)
    }

    private fun initWallLikes() {
        mWallLikeList.layoutManager = LinearLayoutManager(context)
        mWallLikeList.addItemDecoration(DividerItemDecoration(context,
                (mWallLikeList.layoutManager as LinearLayoutManager).orientation))
        val adapter = object : LikeAdapter() {
            override fun loadData(pageNum: Int) {
                RemoteDataQueryObj.queryWallLikes(mWallId, pageNum, 20, object : QueryDoneListener<WallLike> {
                    override fun onFailed(e: Exception) {
                        Log.e(TAG, e.message)
                        onQueryFailed()
                        mDoneRefresh()
                        mDoneRefresh = {}
                    }

                    override fun onSuccess(data: MutableList<WallLike>?) {
                        onQuerySuccess(pageNum, data!!)
                        mDoneRefresh()
                        mDoneRefresh = {}
                    }

                })
            }

        }
        mWallLikeList.adapter = adapter
        mWallLikeList.addOnScrollListener(object : ListEndlessOnScrollListener(mWallLikeList.layoutManager as LinearLayoutManager) {
            override fun isLoading(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>): Boolean =
                    (adapter as LikeAdapter).isLoading

            override fun onLoadMore(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
                (adapter as LikeAdapter).loadMore()
            }
        })

        adapter.loadMore()

    }

    companion object {
        private val TAG = LikeFragment::class.simpleName
        private val ARG_WALL_ID = "wallId"

        fun newInstance(wallId: String): LikeFragment {
            val fragment = LikeFragment()
            val bundle = Bundle()
            bundle.putString(ARG_WALL_ID, wallId)
            fragment.arguments = bundle
            return fragment
        }
    }
}