package cn.edu.xmu.freewill.view.wall.like

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.support.v7.widget.RecyclerView
import android.util.LruCache
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import cn.bmob.v3.datatype.BmobFile
import cn.edu.xmu.freewill.R
import cn.edu.xmu.freewill.data.WallLike
import cn.edu.xmu.freewill.utils.loadThumbnail
import com.makeramen.roundedimageview.RoundedImageView

/**
 * Created by eason on 2018/1/7.
 */
abstract class LikeAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mLikes = mutableListOf<WallLike?>()
    private var mCurrPage = -1
    var isLoading = false
        set(value) {
            if (value) addLoadingView()
            else removeLoadingView()
            field = value
        }

    val mMemoryCache: LruCache<String, Bitmap>

    init {
        val maxMemory = Runtime.getRuntime().maxMemory() / 1024
        val cacheSize = maxMemory / 8
        mMemoryCache = object : LruCache<String, Bitmap>(cacheSize.toInt()) {
            override fun sizeOf(key: String?, bitmap: Bitmap?): Int =
                    if (bitmap == null) 0 else bitmap.byteCount / 1024
        }
    }

    abstract fun loadData(pageNum: Int)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            when (viewType) {
                VIEW_TYPE_LOADING -> {
                    val v = LayoutInflater.from(parent.context)
                            .inflate(R.layout.item_loading, parent, false)
                    LoadingViewHolder(v)
                }
                else -> {
                    val v = LayoutInflater.from(parent.context)
                            .inflate(R.layout.item_wall_like, parent, false)
                    ItemViewHolder(v)
                }
            }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val like = mLikes[position] ?: return
        holder as ItemViewHolder
        holder.timestampView.text = like.createdAt
        holder.usernameView.text = like.user?.username
        val avatar = like.user?.avatar
        if (avatar != null) {
            val avatarImg = mMemoryCache[like.user?.objectId]
            if (avatarImg != null) {
                holder.avatarView.setImageBitmap(avatarImg)
            } else {
                val thumbnail = BmobFile()
                thumbnail.url = avatar.url + "!/fwfh/100x100/format/png"
                thumbnail.loadThumbnail(holder.itemView.context, like.user!!.objectId, {
                    Handler(Looper.myLooper()).post {
                        mMemoryCache.put(like.user!!.objectId, BitmapFactory.decodeFile(it.absolutePath))
                    }
                    holder.avatarView.setImageURI(Uri.fromFile(it))
                })
            }
        }
    }

    override fun getItemViewType(position: Int): Int =
            if (mLikes[position] != null) VIEW_TYPE_ITEM else VIEW_TYPE_LOADING

    override fun getItemCount(): Int = mLikes.size

    protected fun onQuerySuccess(pageNum: Int, likes: MutableList<WallLike>) {
        isLoading = false
        if (pageNum <= mCurrPage) {
            mLikes.clear()
            notifyDataSetChanged()
        }
        if (likes.isNotEmpty()) {
            mCurrPage = pageNum
            mLikes.addAll(likes)
            notifyDataSetChanged()
        }
    }

    protected fun onQueryFailed() {
        isLoading = false
    }

    private fun addLoadingView() {
        mLikes.add(null)
        notifyItemInserted(mLikes.lastIndex)
    }

    private fun removeLoadingView() {
        if (mLikes.isNotEmpty() && mLikes.last() == null) {
            mLikes.removeAt(mLikes.lastIndex)
            notifyItemRemoved(mLikes.lastIndex + 1)
        }
    }

    fun loadMore() {
        if (isLoading) return
        isLoading = true
        loadData(mCurrPage + 1)
    }

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            ButterKnife.bind(this, itemView)
        }

        @BindView(R.id.avatarView)
        lateinit var avatarView: RoundedImageView

        @BindView(R.id.userNameView)
        lateinit var usernameView: TextView

        @BindView(R.id.timeStampView)
        lateinit var timestampView: TextView
    }

    inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            ButterKnife.bind(this, itemView)
        }

        @BindView(R.id.progressBar)
        lateinit var progress: ProgressBar
    }

    companion object {
        private val VIEW_TYPE_LOADING = 0
        private val VIEW_TYPE_ITEM = 1
    }
}